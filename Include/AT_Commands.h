/*
 * AT_Commands.h
 *
 *  CreATed on: 28 Feb 2018
 *      Author:
 */
#ifndef __AT_Commands_h__
#define __AT_Commands_h__


#ifdef __cplusplus
extern "C" {
#endif


#define GENERAL_TIMEOUT				5
#define INPUT_TIMEOUT          		2
#define GPRS_RECEIVE_TIMEOUT		1
#define CONNECT_TIMEOUT				60		//AT+CIPSTART
#define SERVER_RESPONSE_TIMEOUT		645		//AT+CIPSEND
#define SHUT_TIMEOUT				65		//AT+CIPSHUT
#define INIT_TIMEOUT				85		//AT+CIICR
#define SIM_PIN_TIMEOUT				5
#define CGATT_TIMEOUT				10
#define GPRS_TIMEOUT				20
#define CLOSE_TIMEOUT				5


/* =========================================================================
 *		Declaration of all the commands that the SIM80x Module
 *						is going to use
 * =========================================================================
 */
	/*!
	* \brief
	*	A struct that contains the necessary AT Commands
	*	to establish TCP/IP and GPRS CommunicATion.
	*	All the pages thAT are referred to below are from the SIM800 Series_AT Command Manual_V1.09.pdf
	*************************************************************************************************************************
	*
	*	_Gen_mntr_pwr_volt 		= "AT+CBC"      ;	Monitor the power voltage											    *
	*	_Gen_pwr_off 			= "AT+CPOWD"    ;	Power Off by AT Command (1: Power off)									*
	*	_Gen_sleep_mode			= "AT+CSCLK"    ;	0: Normal, 1: Sleep Mode												*
	*	_Gen_netlight_ind  		= "AT+CSGS"     ;	Netlight IndicATion of GPRS												*
	*	_Gen_check_qos			= "AT+CSQ"	    ;	Check Quality of Service												*
	*	_Gen_serial_number		= "AT+CGSN"		;   Serial Number of the Module					---->  see p.68				*
	*	_Gen_send_data			= "AT+CIPSEND"	;  	Sends data after a connection is established. DATa can be sent only		*
	*										 		after the '>' character is received as a response from the module.		*
	*										 		A delimeter CTRL-Z (HEX 0x1A or DEC 26) must be sent as the				*
	*										 		last character. Otherwise, the total length of the sending file.		*														*
	*	_Gen_conf_as_server	 	= "AT+CIPSERVER";	Configure Module SIM80x as Server			---->  see p.236,237		*
	*																														*
	*	_Gen_IP_get_local		= "AT+CIFSR"	;	Get Local IP Address, RESPONSE is <IP Address>							*
	*	_Gen_con_status 		= "AT+CIPSTATUS";   Query Current Connection StATus				---->  see p.231,232		*
	*																														*
	*	._Gen_error_report 	 	= "AT+CMEE"		;	Get the error report of mobile equipment								*
	*													0: Disable +CME err													*
	*													1: Enable  +CME err and use numeric values (default use)			*
	*													2: Enable  +CME err and use verbose values							*
	*	._Gen_set_echo_mode		= "ATE1"		; 	Disable echo															*
	*	._Gen_detect_switch 	= "AT+CSDT"		;	0: Disable detecting SIM card,											*
	*												1: Enable detecting SIM card.											*
	*	._Gen_store_profile		= "AT&W0"		;	Stores current parameter setting in the user profile 0.					*
	*	._Gen_timestamp			= "AT+CCLK?"	;	Get the timestamp														*																													*
	*************************************************************************************************************************
	*
	*	_Init_AT				= "AT"			;	Ping the module to see if it's up
	*	_Init_set_funct			= "AT+CFUN"		;	First Parameter: Set Functionality 	0: Minimum Functionality
	*																					1: Full Functionality,
	*																					4: Disable phone both transmit and receive RF circuits
	*												Second Parameter(optional): Reset   1: Reset the MT before setting it to <fun> power level
	*	_Init_br				= "AT+IPR"		;	Set fix baud rATe
	*	_Init_enter_PIN			= "AT+CPIN"		;   Enter PIN, PUK, etc.
	*	_Init_change_pwrd		= "AT+CPWD"		;	Change Pin Code
	*
	***************************************************************************************************************************
	*
	*	_GPRS_netlight_status   = "AT+CSGS",	;	Find the registration status without using CREG command
	*	_GPRS_ntwrk_reg			= "AT+CREG"		;	Info About RegistrATion StATus     			---->  see p.91
	*	_GPRS_op_sel			= "AT+COPS"		; 	OperATor Selection				   			---->  see p.80,81
	*	_GPRS_start_con			= "AT+CIICR"	;   Start Wireless Connection with GPRS			---->  see p.230
	*	_GPRS_deact_PDP			= "AT+CIPSHUT"	;   Deactivate GPRS PDP Context					---->  see p.227,228
	*	_GPRS_CSD_con_mode		= "AT+CIPCSGP"  ;	Set CSD or GPRS Connection Mode				---->  see p.237
	*	_GPRS_Attach_or_detach	= "AT+CGATT"	;   ATtach or Detach from GPRS Service			---->  see p.209,210
	*													0: Detach
	*													1: ATtach
	*	_PDP_define				= "AT+CGDCONT"	;   Define PDP Context
	*
	****************************************************************************************************************************
	*
	*	_TCP_Init				= "AT+CIPSTART"	;   Start TCP or UDP Connection
	* 													<mode: "TCP" or "UDP">
	*													<IP address> remote server IP address
	* 													<port> remote server port
	* 													<domain name> remote server domain name
	* 													<stATe> indicATes the progress of connecting
	* 												When module is in multi-IP state, before this command is executed, it
	*												is necessary to process "AT+CSTT, AT+CIICR, AT+CIFSR".
	*	_TCP_set_loc_port		= "AT+CLPORT"	;   Set Local Port (any number from 1-65535)	---->  see p.228
	*	_TCP__close				= "AT+CIPCLOSE" ;   Close the TCP Connection					---->  see p.227
	*													0: Slow Close  -> Steady Connection
	*													1: Quick Close -> Close immediately the connection
	*	_TCP_set_prompt			= "AT+CIPSPRT=" ;   Set Prompt of ‘>’ When Module Sends DATa    ---->  see p.235
	*	_TCP_APN_usr_pwd		= "AT+CSTT"		;   Set APN, user name, password				---->  see p.229,230
	*	_TCP_IP__mode 			= "AT+CIPMODE"	;	Select TCP IP Application Mode, default is norma mode.
	*													0: Normal
	*													1: Transparent
	*	_TCP_IP_start_con		= "AT+CIPMUX"	;   Start up Single or Multiple IP Connection
	*													0: Single Connection
	*													1: Multiple Connection
	*
	*	_TCP_IP_trans_mode		= "AT+CIPQSEND"	;	Select Transmission Mode					---->  see p.225
	*													0: Normal mode
	*													1: Quick Send Mode)
	*
	*	_TCP_recv_manually		= "AT+CIPRXGET" ;	Get DATa from Network Manually				----> see p.243,244
	*													0: auto
	*													1: manually 	Should be set before the conenction
	*													2: The module can get dATa, but the length of output dATa
	*													   cannot exceed 1460 bytes AT a time
	*													3: Similar to mode 2, but in HEX mode, which means the
	*													   module can get 730 bytes maximum AT a time
	*													4: Query how many dATa are not read with a given ID
	*	_TCP_IP_header	 		= "AT+CIPHEAD"  ;	Add IP Header to Receive DATa
	*													0: not add IP Header
	*													1: add with the formAT +IPD,<dATa_length> for single connection
	*	_TCP_prev_con_query		= "AT+CIPACK"   ; 	Query Previous Connection DATa Transmitting StATe
	*												RESPONSE: +CIPACK:<txlen>,<acklen>,<nacklen>
	*													<txlen> The dATa amount which has been sent
	*													<acklen> The dATa amount confirmed successfully by the server
	*													<nacklen> The dATa amount without confirmATion by the server
	*
	*
	*************************************************************************************************************************
	*
	*	_GPS_location_time		= "AT+CIPGSMLOC"
	*
	*************************************************************************************************************************
	*
	*	_HTTP_Init				= "AT+HTTPINIT"
	*	_HTTP_Close				= "AT+HTTPTERM"
	*	_HTTP_Param				= "AT+HTTPPARA"
	*	_HTTP_Data				= "AT+HTTPDATA"
	*	_HTTP_Method_action		= "AT+HTTPACTION"
	*	_HTTP_Read_server_resp	= "AT+HTTPREAD"
	*	_HTTP_Save				= "AT+HTTPSCONT"
	*	_HTTP_Status			= "AT+HTTPSTATUS"
	*	_HTTP_Header			= "AT+HTTPHEAD"
	*
	*************************************************************************************************************************
	*
	*	_SMS_Delete				= "AT+CMGD"
	*	_SMS_Msg_format			= "AT+CMGF"
	*	_SMS_Msg_list			= "AT+CMGL"
	*	_SMS_Read				= "AT+CMGR"
	*	_SMS_Send				= "AT+CMGS"
	*	_SMS_Write_to_Mem		= "AT+CMGW"
	*	_SMS_Send_from_Mem		= "AT+CMSS"
	*	_SMS_Delete_all			= "AT+CMGDA"
	*
	*************************************************************************************************************************
	*
	*	_CALL_Answer			= "ATA"
	*	_CALL_Dial				= "ATD"
	*	_CALL_Redial			= "ATDL"
	*
	*************************************************************************************************************************
	*
	*	_EMAIL_Bearer			= "AT+EMAILCID"
	*	_EMAIL_Timeout			= "AT+EMAILTO"
	*	_EMAIL_Srv_addr_port	= "AT+SMTPSRV"
	*	_EMAIL_Authentication	= "AT+SMTPAUTH"
	*	_EMAIL_Sender_info		= "AT+SMTPFROM"
	*	_EMAIL_Receiver_info	= "AT+SMTPRCPT"
	*	_EMAIL_Subject			= "AT+SMTPSUB"
	*	_EMAIL_Body				= "AT+SMTPBODY"
	*	_EMAIL_File_attach		= "AT+SMTPFILE"
	*	_EMAIL_Send				= "AT+SMTPSEND"
	*
	*	_IP_Bearer				= "AT+SAPBR"
	*	_MIC_Open_Close			= "AT+CEXTERNTONE"
	*	_EDGE_Enable			= "AT+CEGPRS"
	*
	*************************************************************************************************************************
	*
	*	_DNS_conf 				= "AT+CDNSCFG"  ;	Configure Primary and Secondary Domain Name Server
	*													Parameters:  <pri_dns> IP Address of the primary DNS
	*																 <sec_dns> IP Address of the secondary DNS
	*	_DNS_IP_query 			= "AT+CDNSGIP"	;	Query the IP Address of Given Domain Name, Parameter: <domain name>
	*
	*************************************************************************************************************************

*/

typedef struct 
{
	// General AT commands
	char* _Gen_mntr_pwr_volt;
	char* _Gen_pwr_off;
	char* _Gen_sleep_mode;
	char* _Gen_netlight_ind;
	char* _Gen_factory_reset;
	char* _Gen_check_qos;
	char* _Gen_serial_number;
	char* _Gen_send_data;
	char* _Gen_conf_as_server;
	char* _Gen_IP_get_local;
	char* _Gen_con_status;
	char* _Gen_error_report;
	char* _Gen_set_echo_mode;
	char* _Gen_detect_switch;
	char* _Gen_store_profile;
	char* _Gen_timestamp;
	char* _Gen_set_time;
	char* _Gen_ntp;
	char* _Gen_Loc_Date;
	char* _Gen_bearer_profile;

	// Initialization AT commands
	char* _Init_AT;
	char* _Init_set_funct;
	char* _Init_br;
	char* _Init_enter_PIN;
	char* _Init_change_pwrd;

	// GPRS AT commands
	char* _GPRS_netlight_status;
	char* _GPRS_ntwrk_reg;
	char* _GPRS_op_sel;
	char* _GPRS_start_con;
	char* _GPRS_deact_PDP;
	char* _GPRS_CSD_con_mode;
	char* _GPRS_attach_or_detach;
	char* _PDP_define;
	char* _GPRS_bearer_sets;
	// TCP AT commands
	char* _TCP_Init;
	char* _TCP_set_loc_port;
	char* _TCP_close;
	char* _TCP_set_prompt;
	char* _TCP_APN_usr_pwd;
	char* _TCP_IP_start_con;
	char* _TCP_IP_mode;
	char* _TCP_IP_trans_mode;
	char* _TCP_recv_manually;
	char* _TCP_IP_header;
	char* _TCP_prev_con_query;

	// DNS AT commands
	char* _DNS_conf;
	char* _DNS_IP_query;

	// Location (GPS) AT command
	char* _GPS_location_time;

	// HTTP AT commands
	char* _HTTP_Init;
	char* _HTTP_Close;
	char* _HTTP_Param;
	char* _HTTP_Data;
	char* _HTTP_Method_action;
	char* _HTTP_Read_server_resp;
	char* _HTTP_Save;
	char* _HTTP_Status;
	char* _HTTP_Header;

	// SMS AT commands
	char* _SMS_Delete;
	char* _SMS_Msg_format;
	char* _SMS_Msg_list;
	char* _SMS_Read;
	char* _SMS_Send;
	char* _SMS_Write_to_Mem;
	char* _SMS_Send_from_Mem;
	char* _SMS_Delete_all;

	// CALL AT commands
	char* _CALL_Answer;
	char* _CALL_Dial;
	char* _CALL_Redial;

	// EMAIL AT commands
	char* _EMAIL_Bearer;
	char* _EMAIL_Timeout;
	char* _EMAIL_Srv_addr_port;
	char* _EMAIL_Authentication;
	char* _EMAIL_Sender_info;
	char* _EMAIL_Receiver_info;
	char* _EMAIL_Subject;
	char* _EMAIL_Body;
	char* _EMAIL_File_attach;
	char* _EMAIL_Send;

	// Extra useful AT commands
	char* _IP_Bearer;
	char* _MIC_Open_Close;
	char* _EDGE_Enable;
} AT_t;



#define __Init_AT_st(s) AT_t s = {					\
													\
		/*	General AT commands*/					\
		._Gen_mntr_pwr_volt 	= "AT+CBC",			\
		._Gen_pwr_off 			= "AT+CPOWD",		\
		._Gen_sleep_mode		= "AT+CSCLK",		\
		._Gen_netlight_ind  	= "AT+CSGS",		\
		._Gen_factory_reset 	= "AT&F0",			\
		._Gen_check_qos			= "AT+CSQ",			\
		._Gen_serial_number		= "AT+CGSN",		\
		._Gen_send_data			= "AT+CIPSEND",		\
		._Gen_conf_as_server	= "AT+CIPSERVER",	\
		._Gen_con_status 		= "AT+CIPSTATUS",	\
		._Gen_IP_get_local		= "AT+CIFSR",		\
		._Gen_set_echo_mode 	= "ATE0",			\
		._Gen_error_report  	= "AT+CMEE",		\
		._Gen_detect_switch 	= "AT+CSDT",		\
		._Gen_store_profile 	= "AT&W0",			\
		._Gen_timestamp			= "AT+CCLK?",		\
		._Gen_set_time			= "AT+CLTS",		\
		._Gen_ntp				= "AT+CNTP",		\
		._Gen_Loc_Date			= "AT+CIPGSMLOC",	\
		._Gen_bearer_profile	= "AT+CNTPCID",		\
													\
													\
		/*	Initialization AT commands*/			\
		._Init_AT				= "AT",				\
		._Init_set_funct		= "AT+CFUN",		\
		._Init_br				= "AT+IPR",			\
		._Init_enter_PIN		= "AT+CPIN",		\
		._Init_change_pwrd		= "AT+CPWD",		\
													\
													\
		/*	GPRS AT commands*/						\
		._GPRS_netlight_status  = "AT+CSGS",		\
		._GPRS_ntwrk_reg		= "AT+CREG",		\
		._GPRS_op_sel			= "AT+COPS",		\
		._GPRS_start_con		= "AT+CIICR",		\
		._GPRS_deact_PDP		= "AT+CIPSHUT",		\
		._GPRS_CSD_con_mode		= "AT+CIPCSGP",		\
		._GPRS_attach_or_detach	= "AT+CGATT",		\
		._PDP_define			= "AT+CGDCONT",		\
		._GPRS_bearer_sets		= "AT+SAPBR", 		\
													\
		/*	TCP AT commands*/						\
		._TCP_Init				= "AT+CIPSTART",	\
		._TCP_set_loc_port		= "AT+CLPORT",		\
		._TCP_close				= "AT+CIPCLOSE",	\
		._TCP_set_prompt		= "AT+CIPSPRT",		\
		._TCP_APN_usr_pwd		= "AT+CSTT",		\
		._TCP_IP_mode 			= "AT+CIPMODE",		\
		._TCP_IP_start_con		= "AT+CIPMUX",		\
		._TCP_IP_trans_mode 	= "AT+CIPMODE",		\
		._TCP_recv_manually		= "AT+CIPRXGET",	\
		._TCP_IP_header	 		= "AT+CIPHEAD",		\
		._TCP_prev_con_query	= "AT+CIPACK",		\
													\
													\
		/*	DNS AT commands*/						\
		._DNS_conf 				= "AT+CDNSCFG",		\
		._DNS_IP_query 			= "AT+CDNSGIP",		\
													\
													\
		/*	Location (GPS) AT command*/				\
		._GPS_location_time		= "AT+CIPGSMLOC",	\
													\
													\
		/*	HTTP AT commands*/						\
		._HTTP_Init				= "AT+HTTPINIT",	\
		._HTTP_Close			= "AT+HTTPTERM",	\
		._HTTP_Param			= "AT+HTTPPARA",	\
		._HTTP_Data				= "AT+HTTPDATA",	\
		._HTTP_Method_action	= "AT+HTTPACTION",	\
		._HTTP_Read_server_resp	= "AT+HTTPREAD",	\
		._HTTP_Save				= "AT+HTTPSCONT",	\
		._HTTP_Status			= "AT+HTTPSTATUS",	\
		._HTTP_Header			= "AT+HTTPHEAD",	\
													\
													\
		/*	SMS AT commands*/						\
		._SMS_Delete			= "AT+CMGD",		\
		._SMS_Msg_format		= "AT+CMGF",		\
		._SMS_Msg_list			= "AT+CMGL",		\
		._SMS_Read				= "AT+CMGR",		\
		._SMS_Send				= "AT+CMGS",		\
		._SMS_Write_to_Mem		= "AT+CMGW",		\
		._SMS_Send_from_Mem		= "AT+CMSS",		\
		._SMS_Delete_all		= "AT+CMGDA",		\
													\
													\
		/*	CALL AT commands*/						\
		._CALL_Answer			= "ATA",			\
		._CALL_Dial				= "ATD",			\
		._CALL_Redial			= "ATDL",			\
													\
													\
		/*	EMAIL AT commands*/						\
		._EMAIL_Bearer			= "AT+EMAILCID",	\
		._EMAIL_Timeout			= "AT+EMAILTO",		\
		._EMAIL_Srv_addr_port 	= "AT+EMAILTO",		\
		._EMAIL_Authentication	= "AT+SMTPAUTH",	\
		._EMAIL_Sender_info		= "AT+SMTPFROM",	\
		._EMAIL_Receiver_info	= "AT+SMTPRCPT",	\
		._EMAIL_Subject			= "AT+SMTPSUB",		\
		._EMAIL_Body			= "AT+SMTPBODY",	\
		._EMAIL_File_attach		= "AT+SMTPFILE",	\
		._EMAIL_Send			= "AT+SMTPSEND",	\
													\
													\
		/*	Extra useful AT commands*/				\
		._IP_Bearer				= "AT+SAPBR",		\
		._MIC_Open_Close		= "AT+CEXTERNTONE",	\
		._EDGE_Enable			= "AT+CEGPRS",		\
}




/* ===============================================================
 * 	  Declaration of the standard the AT responses the SIM80x
 * 		     Module is going to send after an AT Command
 * ===============================================================
 */

/*!
 * \brief
 *	An explanation of the below responses from the SIM80x module
 *
 *
 *		_Gen_ok			  	 = 	 "OK";				The AT command was successful
 *		_Gen_accept	 		 = 	 "ACCEPT";
 *		_Gen_already		 =   "ALREADY";
 *		_Gen_send_ok		 = 	 "SEND OK";			If CIPQSEND:0 and the sent was successful
 *		_Gen_SMS_ready		 =   "SMS Ready";		After successful connection to the network instead of an OK response, this
 *													is considered the positive response of the module
 *		_Gen_Chinese		 =   "CaII Ready";		Some nonsense the Sim80x sends as a positive response
 *		_Gen_error  		 =   "ERROR";			The AT command was unsuccessful
 *		_Gen_connect_ok	 	 = 	 "CONNECT OK";		If Connection was successful with AT+CIPSTART
 *		_Gen_send_fail		 = 	 "SEND FAIL";		The transmission from the module was unsuccessful
 *		_Gen_get_prompt 	 = 	 ">";				The character '>' after the AT+CIPSEND command means thAT
 *													the module is ready to send the dATa through TCP-UDP connection
 *		._Gen_TCP_UDP_connect= 	 "CONNECT OK";		After the CIPSTART and the OK response, when the connection is truly
 *													established the response CONNECT OK is received
 *		_Gen_close			 =	 "CLOSE OK";		If AT+CIPCLOSE is successful
 *		_Gen_shut_ok		 =   "SHUT OK";			For the AT+CIPSHUT , deactivATe GPRS Context
 *		_Gen_already_connect =   "ALREADY CONNECT"; If the module has already established connection
 *		_Gen_busy			 =   "BUSY";		  	This implies that the module is busy at the moment


 */
typedef struct
{
	char* _Gen_ok;
	char* _Gen_accept;
	char* _Gen_already;
	char* _Gen_send_ok;
	char* _Gen_SMS_ready;
	char* _Gen_Chinese;
	char* _Gen_error;
	char* _Gen_noanswer;
	char* _Gen_fail;
	char* _Gen_busy;
	char* _Gen_nocarrier;
	char* _Gen_TCP_UDP_connect;
	char* _Gen_prompt;
	char* _Gen_TCP_close;
	char* _Gen_GPRS_shut;
	char* _Gen_ready;
	char* _Gen_already_con;
} responses_t;


#define __Init_responses_st(s) responses_t s = { \
		._Gen_ok 			 = "OK",		    	\
		._Gen_accept		 = "ACCEPT",	    	\
		._Gen_already		 = "ALREADY",	    	\
		._Gen_send_ok		 = "SEND OK",	    	\
		._Gen_SMS_ready		 = "SMS Ready",	    	\
		._Gen_Chinese		 = "CaII Ready",		\
		._Gen_error			 = "ERROR",		    	\
		._Gen_noanswer		 = "NO ANSWER",	    	\
		._Gen_fail	 		 = "FAIL",	  	    	\
		._Gen_busy		 	 = "BUSY",	   	    	\
		._Gen_nocarrier	 	 = "NO CARRIER",    	\
		._Gen_TCP_UDP_connect= "CONNECT OK",    	\
		._Gen_TCP_close		 = "CLOSE OK",			\
		._Gen_GPRS_shut		 = "SHUT OK",			\
		._Gen_prompt 	 	 = ">",			    	\
		._Gen_ready		 	 = "READY",		    	\
		._Gen_already_con	 = "ALREADY CONNECT",	\
}


//extern const AT_st_t AT_st;
//extern const responses_st_t rsp_st;

#ifdef __cplusplus
}
#endif

#endif
