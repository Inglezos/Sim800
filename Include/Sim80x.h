/*
 * Sim80x.h
 *
 *  Created on: 27 Feb 2018
 *      Author:
 */


#ifndef __Sim80x_h__
#define __Sim80x_h__



#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <AT_Commands.h>
#include <RN_GSM2M.h>
#include <string.h>
#include <math.h>
#include <toolbox.h>
#include <time.h>


#define HASH_SIZE 		(23)		// The length of the union that contains the error messages
#define ATTEMPTS_NUM	(1000)		// The max num of attempts to execute correctly a command
//#define DEBUG


/* Type definitions for pointers to functions*/

typedef int (*Sim80x_getchar_ft) (void); 		/*!< Function pointer to a getchar function      */
typedef int (*Sim80x_putchar_ft) (char); 		/*!< Function pointer to a putchar function      */
typedef int (*Sim80x_CheckReceive_ft) (void); 	/*!< Function pointer to a CheckReceive fuction  */
typedef void (*Sim80x_ReceiveFlush_ft) (void); 	/*!< Function pointer to a ReceiveFlush function */

/* Structure used as io with all the function pointers required for the Sim80x module */
typedef struct {
	Sim80x_getchar_ft 			Getchar;
	Sim80x_putchar_ft 			Putchar;
	Sim80x_CheckReceive_ft 		CheckReceive;
	Sim80x_ReceiveFlush_ft		ReceiveFlush;
} sim80x_io_t;



#ifndef __weak
#define __weak __attribute__ ((weak))
#endif


/*!
 * \brief
 * 	This array contains the response error codes from the SIM80x module
 * 	due to a malfunction in the SIM Card.
 */
typedef union {
	struct {
		char* _Phone_failure;
		char* _No_connection;
		char* _Network_error;
		char* _DNS_common_error;
		char* _SIM_not_inserted;
		char* _PIN_required;
		char* _PUK_required;
		char* _SIM_failure;
		char* _SIM_busy;
		char* _SIM_wrong;
		char* _Password_wrong;
		char* _PIN2_required;
		char* _PUK2_required;
		char* _Memory_full;
		char* _No_network;
		char* _Unknown_error;
		char* _GPRS_unknown_error;
		char* _GPRS_denied;
		char* _PDP_failure;
		char* _Phone_busy;
		char* _User_abort;
		char* _SIM_blocked;
		char* _SIM_powered_off;
	} name;
	char* number [HASH_SIZE];
} SIM_Equipment_Error_msgs_t;



#define __Init_SIM_Equipment_Error_msgs_st(s) SIM_Equipment_Error_msgs_t s = {		\
		.name = {																	\
		._Phone_failure 		= "Phone failure",									\
		._No_connection 		= "No connection to phone",							\
		._Network_error			= "Network Error",									\
		._DNS_common_error		= "DNS common error",								\
		._SIM_not_inserted		= "SIM not inserted",								\
		._PIN_required			= "SIM PIN required",								\
		._PUK_required			= "SIM PUK required",								\
		._SIM_failure			= "SIM failure",									\
		._SIM_busy				= "SIM busy",										\
		._SIM_wrong				= "SIM wrong",										\
		._Password_wrong		= "Incorrect password",								\
		._PIN2_required			= "SIM PIN2 required",								\
		._PUK2_required			= "SIM PUK2 required",								\
		._Memory_full			= "Memory full",									\
		._No_network			= "No network service",								\
		._Unknown_error			= "Unknown error",									\
		._GPRS_unknown_error	= "Unspecified GPRS error",							\
		._GPRS_denied			= "GPRS services not allowed",						\
		._PDP_failure			= "PDP authentication failure",						\
		._Phone_busy			= "Phone is busy",									\
		._User_abort			= "User abort",										\
		._SIM_blocked			= "SIM Blocked",									\
		._SIM_powered_off		= "SIM powered down",								\
		}																			\
}



 /*!
 * \brief
 *	A struct that is going to be used from a Hash Table
 *	to connect an error message and an error code in a
 *	single structure
 */
typedef struct {
	char* data;
	int  key;
}hash_t;



/*!
 * \brief
 *	Enumerator for the possible scenarios of the Response
 *  from the Sim80x module
 *
 *  	- SIM_ERROR, general error
 *  	- SIM_FUN_ERROR, in case an error during the Initialization function occurs
 *	 	- GPRS_ERROR, if the init of GPRS fails or any of the GPRS operations
 *	 	- EMPTY, if the receive data function has an empty buffer
 *	 	- TIMEOUT,
 *	 	- OK,
 *	 	- CONNECT_OK,
 *	 	- ALREADY_CONNECTED,
 *	 	- CLOSE_OK,
 *	 	- SHUT_OK,
 *	 	- SMS_Ready, SIM80x module spouts a bunch of jargon. Hence, we use it to spot the invalid message (done after trial and error)
 *	 	- CHINESE, bunch of jargon that the module spouts. We use it as valid response (after trial and error)
 *	 	- READY,
 *	 	- FULL_BUFFER,
 *	 	- PROMPT,
 *	 	- DNS_ERROR,
 *	 	- OTHER, used in Sim80x_check_response()
 */
typedef enum {
	 SIM80_SIM_ERROR = 0,
	 SIM80_SIM_FUN_ERROR,
	 SIM80_GPRS_ERROR,
	 SIM80_EMPTY,
	 SIM80_TIMEOUT,
	 SIM80_OK,
	 SIM80_CONNECT_OK,
	 SIM80_SEND_OK,
	 SIM80_ALREADY_CONNECTED,
	 SIM80_CLOSE_OK,
	 SIM80_SHUT_OK,
	 SIM80_SMS_Ready,
	 SIM80_CHINESE,
	 SIM80_READY,
	 //SIM80_FULL_BUFFER,
	 SIM80_PROMPT,
	 SIM80_DNS_ERROR,
	 SIM80_OTHER,
} handle_status_en;


/*!
 * An enumarator for the hash_map
 *  - HASH_ERROR, used in hash_insert()
 *  - HASH_OK, everything during the insertion worked perfectly
 */

typedef enum {
	HASH_ERROR,
	HASH_OK
} hash_en;

/*!
 * Used in Sim80x_send mainly in order to  select what to send: data or an AT command
 */
typedef enum {
	AT_COMMAND,
	DATA
}send_mode_en;


/*!
 * The SIM codes that can be sent from the Sim80x module
 * 	- SIM_READY
 * 	- SIM_PIN, if still the pin is not inserted
 * 	- SIM_PUK, if the PUK is not inserted after a false insertion of the PIN
 * 	- PH_SIM_PIN
 * 	- PH_SIM_PUK
 * 	- SIM_PIN2
 * 	- SIM_PUK2
 * 	- SIM_PIN_ERROR, if the inserted PIN was wrong
 */
typedef enum {
	SIM_READY,
	SIM_PIN,
	SIM_PUK,
	PH_SIM_PIN,
	PH_SIM_PUK,
	SIM_PIN2,
	SIM_PUK2,
	SIM_PIN_ERROR
} SIM_codes_en;

typedef enum {
	 TCP,
	 UDP
 } protocol_en;

 typedef enum {
    DETACHED,
 	ATTACHED
 } attach_detach_en;


 /* Number of total responses received from the Sim80x module.
  * 	- RESPONSES 1:  e.g. \r\nOK\r\n
  * 	- RESPONSES 2:  e.g. \r\nOK\r\n\r\nCONNECT OK\r\n
  */
 typedef enum {
  	RESPONSES1,
	RESPONSES2
 } rsps_num_en;


 /* Quality of the signal */
 typedef enum {
	MARGINAL,
	BAD,
	GOOD,
	HIGH,
	EXCELLENT,
	SUPER_EXCELLENT,
	UNKNOWN
 } QoS_en;

/* Bit Error Rate */
typedef enum {
	EXTRA_HIGH_ERROR_RATE,
	HIGH_ERROR_RATE,
	LOW_ERROR_RATE,
	EXTRA_LOW_ERROR_RATE
} BER_en;


typedef enum {
	ENABLED_NETWORK,
	DISABLED_NETWORK,
	UNREGISTERED,
	REGISTERED,
	SEARCHING,
	DENIED,
	UNKNOWN_NETWORK,
	ROAMING
} registration_en;


typedef enum
 {
	SERVER_SETTINGS_APPLIED,
	SERVER_SETTINGS_ERROR,
	SERVER_SETTINGS_NOT_FOUND
} server_settings_en;


typedef struct {
	float longitude;
	float latitude;
} location_t;

/* A struct that contains the features that are extracted from the Sim80x module
 * These include the IP of the server, the DNS, the PORT in which the connection
 * is established etc.
 */
typedef struct {
	char IPandPORT[30];
	char DNS[50];
	char DNSandIP[50];
	char localIP[20];
	char IP1[20];		/*< The first IP that the AT+CDNSGIP=e.t.c. sends */
	char IP2[20];		/*< The second IP that sometimes the AT+CDNSGIP commands sends */
	char SIM_code[30];

	unsigned int PORT;
	location_t location;
	int QoS;
	int BER;
	int send_length;
	int txlen;
	int acklen;
	int nacklen;
	int reg_code;
	int reg_status;
} extracted_features_t;


/*! A struct in which the strings for
 * 	- APN, max 31 letters  (e.g. "internet.vodafone.gr", the \" characters are  necessary)
 * 	- Domain, max 63 letters  (e.g. "www.my_first_attempt_to_write_good_code.gr")
 * 	- protocol, 5 letters  ("TCP" or "UDP")
 * 	- port, number of port (max num of port is "65535")
 * 	- Gen_dns_com_info, the total array that includes all of the above (e.g. 107 = 49+49+5+7 + 1 for the '\0's)
 */
typedef struct {
	char sim_apn[32];
	char sim_domain[64];
	char protocol[6];
	char port[8];
	char Gen_dns_com_info[111];
} GPRS_t;


/* The main struct of the GSM Sim80x module.
 * It contains pointers
 * 		- pin
 * 		- new_pin
 * 		- the baud rate
 * 		- the possible error messages,
 *      - the IP_Header of the received message
 *      - the received data size
 *      - an array of the error numbers
 *      - a pointer to the AT_t structure
 *      - a pointer to the responses_t structure
 *      - a pointer to the extracted_features_t struct
 */
typedef struct {
   char* 				 		pin;
   char* 				 		new_pin;
   char* 				 		br;
   char* 					 	err_msg;
   char 				 		ip_head[15];
   char							phone_number[20];
   int 					 		recvdData_size;
   const int* 			 		error_numbers;
   AT_t* 				 		AT;
   responses_t* 		 		responses;
   extracted_features_t* 	    extracted_features;
   GPRS_t* 					    GPRS;
   SIM_Equipment_Error_msgs_t*  SIM_Equipment_Error_msgs;
   sim80x_io_t 					io;
} sim80x_t;



/* Link and Glue functions for every function pointer used */
void Sim80x_link_putchar (sim80x_t *sim80x, Sim80x_putchar_ft fun);
void Sim80x_link_getchar (sim80x_t *sim80x, Sim80x_getchar_ft fun);
void Sim80x_link_CheckReceive (sim80x_t *sim80x, Sim80x_CheckReceive_ft fun);
void Sim80x_link_ReceiveFlush (sim80x_t *sim80x, Sim80x_ReceiveFlush_ft fun);



void Sim80x_test_gen (void);
void Sim80x_test (void);

handle_status_en Sim80x_test_recv_Data (sim80x_t* sim80x, char* command, float input_timeout);
//handle_status_en test_GPRS_DNS_IP_query (sim80x_t* sim80x, char *param, float input_timeout);

//char* hash_search(hash_t* hash_table, int key);
//hash_en hash_insert(hash_t* hash_table, char* err_msg, int key);

handle_status_en Sim80x_Init (sim80x_t* sim80x, extracted_features_t *extracted_features, \
							   GPRS_t *GPRS, int mode, float input_timeout);

handle_status_en Sim80x_error_report (sim80x_t* sim80x, char* param, float input_timeout);
handle_status_en Sim80x_SIM_detect (sim80x_t* sim80x, char* param, float input_timeout);
handle_status_en Sim80x_store_profile (sim80x_t* sim80x, float input_timeout);
handle_status_en Sim80x_Sapbr (sim80x_t* sim80x, char* param, float input_timeout);

handle_status_en Sim80x_send_AT_Command (sim80x_t *sim80x, char* command, char c);
handle_status_en Sim80x_send_AT_Command_with_parameters (sim80x_t *sim80x, char* command, char* parameters);
handle_status_en Sim80x_send (sim80x_t *sim80x, void* dataSend, size_t size, send_mode_en send_mode);
//handle_status_en Sim80x_recv_Data(sim80x_t* sim80x, char* command, rsps_num_en mode, float input_timeout);
//handle_status_en Sim80x_check_response(sim80x_t* sim80x);
handle_status_en Sim80x_send_SMS (sim80x_t* sim80x, char* phone_number, char* SMS, float input_timeout);


handle_status_en Sim80x_set_echo_mode (sim80x_t* sim80x, float input_timeout);

handle_status_en Sim80x_set_AT (sim80x_t* sim80x, int mode, float input_timeout);

void Sim80x_set_PIN (sim80x_t* sim80x, char* pin);
char* Sim80x_get_PIN (sim80x_t* sim80x);

handle_status_en Sim80x_set_functionality (sim80x_t* sim80x, char* c, float input_timeout);
handle_status_en Sim80x_set_registration (sim80x_t* sim80x, char* c, float input_timeout);
handle_status_en Sim80x_get_registration (sim80x_t* sim80x, float input_timeout, registration_en *rstatus);

void Sim80x_set_baud_rate (sim80x_t* sim80x, char* br);
char* Sim80x_get_baud_rate (sim80x_t* sim80x);

void Sim80x_set_location(sim80x_t *sim80x, float longitude, float latitude);
void Sim80x_get_location(sim80x_t *sim80x, float *longitude, float *latitude);

handle_status_en Sim80x_insert_PIN (sim80x_t* sim80x, float input_timeout);
handle_status_en Sim80x_insert_baud_rate (sim80x_t* sim80x, int mode, float input_timeout);

handle_status_en Sim80x_insert_new_PIN (sim80x_t* sim80x, hash_t *hash_table, float input_timeout);

handle_status_en Sim80x_request_pin (sim80x_t* sim80x, int mode, float input_timeout);
handle_status_en Sim80x_request_baud_rate (sim80x_t* sim80x, float input_timeout);

void Sim80x_set_phone_number(sim80x_t *sim80x, char *phone_number);
char *Sim80x_get_phone_number(sim80x_t *sim80x);

void GPRS_set_APN (sim80x_t *sim80x, char *apn_in);
char *GPRS_get_APN (sim80x_t *sim80x);

void GPRS_set_Domain (sim80x_t *sim80x, char *dns_in);
char *GPRS_get_Domain (sim80x_t *sim80x);

void GPRS_set_protocol (sim80x_t *sim80x, char *protocol);
char *GPRS_get_protocol (sim80x_t *sim80x);

void GPRS_set_port (sim80x_t *sim80x, char *port);
char *GPRS_get_port (sim80x_t *sim80x);

handle_status_en GPRS_Init (sim80x_t* sim80x, attach_detach_en *astatus);

handle_status_en GPRS_get_netlight_status (sim80x_t* sim80x, registration_en *rstatus, float input_timeout);
handle_status_en GPRS_start (sim80x_t* sim80x, float input_timeout);
handle_status_en GPRS_attach_detach (sim80x_t* sim80x, char* c, float input_timeout);
handle_status_en GPRS_attach_detach_query (sim80x_t* sim80x, hash_t* hash_table, attach_detach_en* astatus, float input_timeout);
handle_status_en GPRS_start_TCP_UDP_DNS_con (sim80x_t* sim80x, float input_timeout);
handle_status_en GPRS_single_multiple (sim80x_t* sim80x, char* param, float input_timeout);
handle_status_en GPRS_check_signal (sim80x_t* sim80x, QoS_en* qstatus, BER_en* bstatus, float input_timeout);
handle_status_en GPRS_get_local_ip (sim80x_t* sim80x, float input_timeout);
handle_status_en GPRS_TCP_close (sim80x_t* sim80x, float input_timeout);
handle_status_en GPRS_PDP_close (sim80x_t* sim80x, float input_timeout);
handle_status_en GPRS_DNS_IP_query (sim80x_t* sim80x, char *param, float input_timeout);
handle_status_en GPRS_query_previous_transm_status (sim80x_t* sim80x, float input_timeout);
handle_status_en GPRS_ip_head (sim80x_t* sim80x, char* sel_mode, float input_timeout);
handle_status_en GPRS_sel_transm_mode (sim80x_t* sim80x, char* sel_mode, float input_timeout);
handle_status_en GPRS_avail_data_length_to_send (sim80x_t* sim80x, unsigned int* size, float input_timeout);
handle_status_en GPRS_insert_APN (sim80x_t* sim80x, float input_timeout);

int _GPRS_extract_data_size(sim80x_t *sim80x);


handle_status_en Gen_set_timestamp (sim80x_t* sim80x, char* ntp_server, float input_timeout);
handle_status_en Gen_get_timestamp (sim80x_t* sim80x, struct tm* timestamp, float input_timeout) ;
handle_status_en Gen_Loc_and_Date (sim80x_t* sim80x, char* param, float input_timeout);
handle_status_en Gen_BearerProfile (sim80x_t* sim80x, char* param, float input_timeout);

//protocol_en Sim80x_extract_protocol_mode(void);
//handle_status_en Sim80x_extract_ack_status(sim80x_t* sim80x);
//registration_en Sim80x_extract_registration(sim80x_t* sim80x);
//SIM_codes_en Sim80x_extract_SIM_code(sim80x_t* sim80x);
//char* Sim80x_extract_DNS(sim80x_t* sim80x);
//char* Sim80x_extract_IPandPORT(sim80x_t* sim80x, int select);
//QoS_en Sim80x_extract_QoS(sim80x_t* sim80x);
//BER_en Sim80x_extract_BER(sim80x_t* sim80x);
//char* Sim80x_extract_DNSandIP(sim80x_t* sim80x, int select);
//unsigned int Sim80x_extract_length(sim80x_t* sim80x);
//unsigned int Sim80x_extract_err_length(void);

//int GPRS_CheckReceive (sim80x_t *sim80x);
int GPRS_extract_data_size(sim80x_t *sim80x);
handle_status_en GPRS_send_data (sim80x_t* sim80x, void* data, size_t size, float input_timeout);
handle_status_en GPRS_getData (sim80x_t *sim80x, void* getData, size_t size, float input_timeout);
//int GPRS_extract_data_size(sim80x_t *sim80x);
// ...

// testing functions...
handle_status_en test_GPRS_Init (sim80x_t* sim80x, hash_t *hash_table, float input_timeout);
// Or extend GPRS API here
// ...
#ifdef __cplusplus
}
#endif

#endif /* __Sim80x_h__ */





