/*
 * Sim80x.c
 *
 *  Sim80x Library Implementation
 *
 *  Created on: 27 Feb 2018
 *      Author:	User
 */


#include "Sim80x.h"

const __Init_AT_st(AT_st);
const __Init_responses_st(rsp_st);
const __Init_SIM_Equipment_Error_msgs_st(SIM_Equipment_Error_msgs_st);

#ifdef DEBUG
	int l=0;
	int k=0;
#endif


/*
 * =================== Data =====================
*/
char receivedDataBuffer_fromSim80x[GSM_UART_BUFFER_SIZE];				/* Used for storing the received data and process it */
static uint8_t Sim80x_flag = 0;											/* Used for deciding whether to call AT+SAPRB="1,1" or not. */
static char current_command[50];										/* Used for storing the current command before the send */
static char check_receivedDataBuffer_fromSim80x[GSM_UART_BUFFER_SIZE];	/* Used for processing the response received from the Module */
//static char* temp_getData;
//uint32_t k = 0, l=0;



/* ==========================================================================
 * ================		  PRIVATE API DECLARATIONS			=================
 * ==========================================================================
 */

/*!
 * brief
 * 	User must insert the timeout that serves best their goal
 * 	,otherwise, if user selects 0 as input_timeout
 * 	the default timeouts of the commands will be given
 * 	for the execution of the commands.
 */
static int _done=0;
static int _hash_index = 0;

static int _error_numbers[HASH_SIZE] =   { 0, 1, 3, 8, 10, 11, 12, 13, 14,		\
								    15, 16, 17, 18, 20, 30, 100,				\
								    107, 148, 149, 258, 258, 262, 772};

static hash_t _hash_table[HASH_SIZE];
static char* _hash_search (hash_t *_hash_table, int key);
static hash_en _hash_insert (hash_t* _hash_table, char* err_msg, int key);


static handle_status_en _Sim80x_check_response(sim80x_t* sim80x);
static handle_status_en _Sim80x_recv_Data(sim80x_t* sim80x, char* command, rsps_num_en mode, float input_timeout);

static void _Sim80x_recv_filter(int mode);
static void _Sim80x_set_AT_command(char* command);
static float _Sim80x_set_timeout(sim80x_t* sim80x, char* command, float fixed_timeout);
static char* _Sim80x_append(char* command, char c);

static protocol_en _Sim80x_extract_protocol_mode(void);
static handle_status_en _Sim80x_extract_ack_status(sim80x_t* sim80x);
static registration_en _Sim80x_extract_registration(sim80x_t* sim80x);
static SIM_codes_en _Sim80x_extract_SIM_code(sim80x_t* sim80x);
static char* _Sim80x_extract_DNS(sim80x_t* sim80x);
static char* _Sim80x_extract_IPandPORT(sim80x_t* sim80x, int select);
static QoS_en _Sim80x_extract_QoS(sim80x_t* sim80x);
static BER_en _Sim80x_extract_BER(sim80x_t* sim80x);
static char* _Sim80x_extract_DNSandIP(sim80x_t* sim80x, int select);
static unsigned int _Sim80x_extract_length(sim80x_t* sim80x);
static unsigned int _Sim80x_extract_err_length(void);

static handle_status_en _GPRS_DNS_IP_tx_rx_query (sim80x_t* sim80x, char *param, float input_timeout);

static handle_status_en Gen_set_ntp_server (sim80x_t* sim80x, char* ntp_server, float input_timeout);
static handle_status_en Gen_ntp_sync (sim80x_t* sim80x, float input_timeout);
static handle_status_en Gen_set_lcl_timestamp (sim80x_t* sim80x, float input_timeout);



/* =======================================================================
 * ===========           PRIVATE Hash Table Functions         ============
 * =======================================================================
 */

/*
 *\brief
 *	Function that searches through the whole
 *
 *\param _hash_table, an array of hash pointers to a Hash_t struct
 *\param key, the key that i want to use
 *
 *\return
 *	\arg NULL: if the hash_item is not in the table
 *	\arg hash_data: the string that contains the err_msg
 */
static char* _hash_search (hash_t *_hash_table, int key) {
	for(int i=0 ; i<HASH_SIZE; ++i) {
		if (_hash_table[i].key == key)
			return _hash_table[i].data;
		else
			continue;
	}
	return NULL;
}


/*
 *\brief
 *	A function that is used to insert the error message
 *	and the key of the message to the struct
 *
 *	- _hash_index is global variable
 */

static hash_en _hash_insert (hash_t* _hash_table, char* err_msg, int key) {
	if (_hash_index >= HASH_SIZE)
		return HASH_ERROR;

	_hash_table[_hash_index].data = err_msg;
	_hash_table[_hash_index].key = key;
	_hash_index++;
	return HASH_OK;
}




/* ===========================================================================
 * ================           Private API FUNCTIONS 		    ==============
 * ===========================================================================
*/

/*!
* \brief
*	Receive response from the SIM80x module.
*
*\param howManyAnswers, defines the number of possible responses.
*\param command, the AT Command to send to the SIM80x module.
*\param mode, 0 for normal commands, 1 for commands that have 2 responses
*\param input_timeout
*
*\return
*	\arg SIM80_OK
*	\arg SIM80_SIM_ERROR
*	\arg SIM80_TIMEOUT
*	\arg SIM80_EMPTY
*/

static handle_status_en _Sim80x_recv_Data (sim80x_t* sim80x, char* command, rsps_num_en mode, float input_timeout) {
	char cur_char[2];
	int i=0, r1=0, n1=0, r2=0, n2=0, flag = 0; 		/* If the \r\n characters have been spotted
													* for the first time the response starts to be sent
													* from SIM80x module. When the \r\n characters are spotted
													* for the second time, then the data-stream/response has been completed and
													* successfully sent from the SIM80x module.
													*/
	float timeout = _Sim80x_set_timeout(sim80x, command, input_timeout);
//	memset(receivedDataBuffer_fromSim80x, '\0', strlen(receivedDataBuffer_fromSim80x));
//	int j = strlen(receivedDataBuffer_fromSim80x);
//	while (j>=0) {
//		receivedDataBuffer_fromSim80x[j] = '\0';
//		j--;
//	}
	/* Empty the strings in order to receive the new data */
	memset(receivedDataBuffer_fromSim80x, '\0', GSM_UART_BUFFER_SIZE);
	memset(check_receivedDataBuffer_fromSim80x, '\0', GSM_UART_BUFFER_SIZE);
	// We are listening to the sender (server) for response, only for specific time interval, the timeout variable.

	clock_t total, now, mark;
	total = timeout*get_freq();

	if (mode ==RESPONSES1) {

		// GSM_UART_BUFFER_SIZE bytes is the max inner buffer size limit.
	   mark = clock ();
		do {
			cur_char[0] = (char)sim80x->io.Getchar();

			receivedDataBuffer_fromSim80x[i] = cur_char[0];
			check_receivedDataBuffer_fromSim80x[i] = cur_char[0];
			if (cur_char[0] == '\r') {
				if (r1 == 0)
					r1 = 1;
				else
					r2 = 1;
			}
			if (cur_char[0] == '\n') {
				if (n1 == 0)
					n1 = 1;
				else
					n2 = 1;
			}

			if (cur_char[0] == '>') {
				sim80x->io.Getchar();	/* After the '>' character the Sim80x Module sends also a space (' ') character */
				return SIM80_OK;
			}

			flag = r1 * n1 * r2 * n2;
			if (flag) {
//				sim80x->io.ReceiveFlush();
				return SIM80_OK;
			}
			if (timeout == 0  && sim80x->io.CheckReceive() == 0)
				return SIM80_EMPTY;
			++i;
			now = clock();
		} while ((_CLOCK_DIFF (now, mark) <= total));
	}

	else if (mode == RESPONSES2) {
		int rflag=0, nflag=0;
		now = clock();

		// GSM_UART_BUFFER_SIZE bytes is the max inner buffer size limit.
		mark = clock ();
		do {
			cur_char[0] = (char)sim80x->io.Getchar();

			receivedDataBuffer_fromSim80x[i] = cur_char[0];
			check_receivedDataBuffer_fromSim80x[i] = cur_char[0];
			if (cur_char[0] == '\r')
				rflag++;

			if (cur_char[0] == '\n')
				nflag++;

			if (cur_char[0] == '>') {
				sim80x->io.Getchar();
				return SIM80_OK;
			}

			flag = rflag*nflag;
			if ((flag/16) > 0) {
//				sim80x->io.ReceiveFlush();
				return SIM80_OK;
			}
			if (timeout == 0  && sim80x->io.CheckReceive() == 0)
				return SIM80_EMPTY;
			++i;
			now = clock ();
		} while ((_CLOCK_DIFF (now, mark) <= total));

	}
//	sim80x->io.ReceiveFlush();

	return SIM80_TIMEOUT;
}





/*!
* \brief
*	Checks the received response status of the SIM80x module.
*/

static handle_status_en _Sim80x_check_response (sim80x_t* sim80x)  {// Returns 0 for successful and 1 for error. 2 for other responses cases.

	if ((strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_TCP_UDP_connect)))
		return SIM80_CONNECT_OK;

	if ((strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_send_ok)))
		return SIM80_SEND_OK;

	else if ((strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_error)) 			||		\
			(strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_noanswer))  		||		\
			(strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_fail)) 			||		\
			(strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_busy)) 			||		\
			(strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_nocarrier)))
		return SIM80_SIM_ERROR;

	else if ((strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_already_con)))
		return SIM80_ALREADY_CONNECTED;

	else if (strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_prompt))
		return SIM80_PROMPT;

	else if (strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_TCP_close))
		return SIM80_CLOSE_OK;

	else if (strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_GPRS_shut))
		return SIM80_SHUT_OK;

	else if (strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_SMS_ready))
		return SIM80_SMS_Ready;

	else if (strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_Chinese))
		return SIM80_CHINESE;

	else if ((strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_ok)) 		 		||		\
			(strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_accept)) 			||		\
			(strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_already))			||		\
			(strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_send_ok)))
		return SIM80_OK;

	else if (strstr(check_receivedDataBuffer_fromSim80x, sim80x->responses->_Gen_ready))
		return SIM80_READY;

	return SIM80_OTHER;
}


/*!
 * \brief
 *	   Remove the redundant characters \r\n or \r\r\n
 *	   from the command. If the mode is 0, then that means that the command has delimiters of this
 *	   format: \r\n. Otherwise, if the mode is 1 it means that the delimiter of that command is: \r\r\n
 * \param mode
 */
static void _Sim80x_recv_filter (int mode) {

	char* token;
	char temp_received[GSM_UART_BUFFER_SIZE];
	char* delimit;
	uint32_t length;

	for (int i=0; i<GSM_UART_BUFFER_SIZE; ++i) {
		temp_received[i] = receivedDataBuffer_fromSim80x[i];
		receivedDataBuffer_fromSim80x[i] = '\0';
	}

	if (mode == 0)
		delimit = "\r\n";
	else if (mode == 1)
		delimit = "\r\r\n";
	else
		delimit = "\r\n";

	token = strstr(temp_received, delimit);

	switch(mode) {
		case 0:
			token += 2;
			break;
		case 1:
			token += 3;
			break;
		default:
			token += 2;
			break;
	}

	length = strlen(token);
	if (token[length-2] == '\r')
		token[length-2] = '\0';
	strcpy(receivedDataBuffer_fromSim80x, token);
}


/*!
* \brief
*	Set timeouts for the different cases of AT commands, in seconds.
*/

static float _Sim80x_set_timeout (sim80x_t* sim80x, char* command, float fixed_timeout) {
	if (fixed_timeout == 1) {
		if (strcmp(command, sim80x->AT->_TCP_Init) == 0)
			return (float)CONNECT_TIMEOUT;
		else if (strcmp(command, sim80x->AT->_Gen_send_data) == 0)
			return (float)SERVER_RESPONSE_TIMEOUT;
		else if (strcmp(command, sim80x->AT->_GPRS_deact_PDP) == 0)
			return (float)SHUT_TIMEOUT;
		else if (strcmp(command, sim80x->AT->_GPRS_start_con) == 0)
			return (float)INIT_TIMEOUT;
		else
			return (fixed_timeout/1000000);
	}
	else if (fixed_timeout > 0 && fixed_timeout != 1) {
		return fixed_timeout;
	}
	return fixed_timeout;
}


/*!
 * \brief
 * 	Copy the command to the static string current_command
 *
 *\param command, the command to be copied
 */
static void _Sim80x_set_AT_command (char* command) {
	uint32_t command_length = strlen(command);
	for(int i=0; i<command_length; ++i)
	{
		current_command[i] = command[i];
	}
}


/*!
 * \brief
 * 	A static function which appends characters in a string.
 *	If the command the user wants to send is a question towards
 *	the SIM Com module a '?' and a '\r' character will be appended,
 *	only '\r' otherwise.
 *
 *\param command, the command we want to sent with the appended characters
 *\param c, if the '?' is the character we want to append we insert '?' as character.
 *		    If it is something else only the '\r' character will be appended.
 *
 *\return
 *	the current_command string
 */
static char* _Sim80x_append (char* command, char c) {
	uint32_t command_length = strlen(command);
	if (c == '?')
	{
		current_command[command_length] = c;
		current_command[command_length+1] = '\r';
	}
	else
		current_command[command_length] = '\r';
	return current_command;
}



/*!
* \brief
*	Searches the received data for the IP and PORT features.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\param  select, set this 0 for IP only or to 1 for both IP and port.
*\return sim80x->extracted_features.IPandPORT:		a char array, pointing to the IPandPORT array inside the extracted_features structure, inside the sim80x type structure.
*/

/*!
* \brief
*	Searches the received data for the IP and PORT features.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\param  select, set this 0 for IP only or to 1 for both IP and port.
*\return sim80x->extracted_features.IPandPORT:		a char array, pointing to the IPandPORT array inside the extracted_features structure, inside the sim80x type structure.
*/

static char *_Sim80x_extract_IPandPORT(sim80x_t *sim80x, int select) {// Set select to 0 for IP only or to 1 for both IP and port.
											// The user knows the command that he sent, so he expects a respective response.
											// Thus, i.e., if he is expecting for an IP to be returned as response, then he
											// will call the getIP().

	// First of all, we will extract the starting IP address digits -> <255>.255.255.255
	// These can be from 1 to 3 digits in length, i.e. 10.0.0.1 or 0.0.0.0 or 127.0.0.1
	// There are two cases; either the IP address is of the form 255.255.255.255 and nothing else before and after (case 1)
	// or of the form ...","255.255.255.255",... (case 2)

	if (receivedDataBuffer_fromSim80x[0] != '\"') {
		// We are in case 1
		strcpy(sim80x->extracted_features->IPandPORT, receivedDataBuffer_fromSim80x);
		strcpy(sim80x->extracted_features->localIP, receivedDataBuffer_fromSim80x);
		return receivedDataBuffer_fromSim80x;
	}

	// Now we are in case 2...
	char delimit[2] = "\"";
	char temp_received[256];
	char init_temp_received[256];
	for (int i=0; i<256; ++i) {
		temp_received[i] = receivedDataBuffer_fromSim80x[i];
		init_temp_received[i] = temp_received[i];
	}

	strtok(init_temp_received, delimit);

	char *start, *end;
	delimit[0] = '.';
	start = &temp_received[0];
	end = strtok(temp_received, delimit); // The first token is between the beginning of the string and the delimiter string. Essentially, we
										  // need the second token, which points to the first character after the delimiter.
	while( end != NULL ) {
		end = strtok(NULL, delimit); // Now the end points to <255>.255.255"...
		break;
	}
	int index = end - start; // The end pointer indicates the first digit after the '.' in the IP address.
							 // Here, after the first '.' -> 255.255.255"... and the index to <2>55.255.255"...

	int start_index = 1;
	for (; start_index<=4; start_index++) {
		if (init_temp_received[index - start_index] == '\0') // We found a NULL character replaced the "
			break;
	}

	start_index--; // start_index has the length of the first IP section <size(255.)>255.255.255

	// Now the (index - start_index) points to the first digit of the IP -> "<2>55.255.255.255",...
	int i;
	for (i=0; i<start_index; ++i) {
		sim80x->extracted_features->IPandPORT[i] = receivedDataBuffer_fromSim80x[i + (index - start_index)];
	}

	// The IP address is expected to be in the usual form of "255.255.255.255" -> max 15 characters.

	// The IPandPORT has now -> 255.
	int cur_i = i;
	int temp_length = strlen(end);
		for (i=0; i<temp_length; ++i)	{
		sim80x->extracted_features->IPandPORT[i + cur_i] = receivedDataBuffer_fromSim80x[i + index];
	}

	// The IPandPORT has now -> 255.255

	index += temp_length; // The index now points to 255<.>255.255"...
	sim80x->extracted_features->IPandPORT[i + cur_i] = receivedDataBuffer_fromSim80x[index];
	// The IPandPORT has now -> 255.255.

	while( end != NULL ) {
		end = strtok(NULL, delimit); // Now the end points to <255>.255"...
		break;
	}

	index++;	// The index now points to <2>55.255"...
	temp_length = strlen(end);
	int IPandPORTindex = i;
	IPandPORTindex++;	// IPandPORTindex points to the extracted IP -> 255.255.<>
	for (i=0; i<temp_length; ++i)	{
		sim80x->extracted_features->IPandPORT[i + IPandPORTindex] = receivedDataBuffer_fromSim80x[i + index];
	}

	// The IPandPORT has now -> 255.255.255

	index += temp_length;	// The index now points to 255<.>255"...
	IPandPORTindex += i;	// IPandPORTindex points to the extracted IP -> 255.255.255<>

	sim80x->extracted_features->IPandPORT[i + IPandPORTindex] = receivedDataBuffer_fromSim80x[index];

	// The IPandPORT has now -> 255.255.255.

	IPandPORTindex++;	// IPandPORTindex points to the extracted IP -> 255.255.255.<>
	index++;	// The index now points to <2>55"...

	if(end != NULL) {
		end = strtok(NULL, delimit); // Now the end points to <255"...\0> or ...<255\0> (see comment below).
	}

	// Now we have two cases. Either the response is of the form <..."255.255.255.255","<port>"...\0> (case 1) or
	// <...255.255.255.255\0 and nothing else (case 2).
	// Thus, searching for "\"" in the next 4 characters, if we find at least one such character-delimiter,
	// then we are in case 1, otherwise in case 2, without the "

	int final_length;
	int flag_case = 2;
	for (final_length=0; final_length<4; final_length++) {
		if (end[final_length] == '\"') {
			flag_case = 1;
			break;
		}
	}

	if (flag_case == 2) {
		// Then the end points to <255\0>
		for (i=0; i<final_length; ++i)	{ // If for example, the IP is 192.0.0.1, then we put extra \0
										  // (final_length = 0,1,2,3 -> 4 characters, so the extra \0 are [4 - actual IP address
										  // numbers], here 4-1=3) to the IPandPORT (not an error!).
			sim80x->extracted_features->IPandPORT[i + IPandPORTindex] = receivedDataBuffer_fromSim80x[i + index];
		}
		// The IPandPORT has now -> 255.255.255.255\0
		return sim80x->extracted_features->IPandPORT;
	}


	// Otherwise, we continue with case 1 and we have a " character to find.

	if (!select) {
		strcpy(sim80x->extracted_features->localIP, sim80x->extracted_features->IPandPORT);
		return sim80x->extracted_features->IPandPORT;
	}

	index += final_length;	// Index now points to "255.255.255.255<">,"<port>"...
	index += 3;	//Index now points to "255.255.255.255","65535"...

	// The form is: "255.255.255.255","65535"
	end = &receivedDataBuffer_fromSim80x[index]; // Now we point to the first digit of the port. The port is in the range of 0-65535.

	// Now, for the IPandPORT array, we find the position at which we have the first \0 character. This is exactly after
	// the IP address and there we put ','
	char *temp_end;
	if ((temp_end = strstr(sim80x->extracted_features->IPandPORT, "\0"))) {
		int temp_str_index = 0;
		start = &(sim80x->extracted_features->IPandPORT[0]);
		temp_str_index = end - start;
		sim80x->extracted_features->IPandPORT[temp_str_index] = ',';
	}

	delimit[0] = '\"';
	if ((end = strtok(temp_received, delimit)) != NULL) { // For sure there is at least a " character left.
														  // Now end has the <port> value in char * form.
//		int port_length = strlen(end);
//		unsigned int digit = 0;
//		for (int i=0; i<port_length; ++i) {
//			sim80x->extracted_features.IPandPORT[i+16] = receivedDataBuffer_fromSim80x[i+index+14];
//			digit = digit + (sim80x->extracted_features.IPandPORT[i+16] - '0')*pow(10,(port_length-1 - i));
//			// I.e.: for 32723 -> d = 0 + 3*10^4 + 2*10^3 + 7*10^2 + 2*10^1 + 3*10^0 = 30000 + 2000 + 700 + 20 + 3 = 32000 + 723 = 32723.
//		}

		unsigned int digit = 0;
		sscanf (end, "%u", &digit);
		sim80x->extracted_features->PORT = digit;
		return sim80x->extracted_features->IPandPORT;
	}
	return sim80x->extracted_features->IPandPORT;
}


/*!
 * \brief
 *	  This function is used to extract the SIM response from the SIM80x module
 *	  If there are no errors but something has to be
 */
static SIM_codes_en _Sim80x_extract_SIM_code (sim80x_t* sim80x)
{
	char temp_received[20]; // The response is of the form: CPIN: <...>

	for (int i=0; i<20; ++i) {
		temp_received[i] = receivedDataBuffer_fromSim80x[i];
	}

	char delimit[2] = ":";
	char* token;

	token = strtok(temp_received, delimit); // The first token is CPIN
	while( token != NULL ) {
		token = strtok(NULL, delimit);		// The second token is < ...>
		break;
	}

	token++;

	strcpy(sim80x->extracted_features->SIM_code, token);

	if (!strcmp(sim80x->extracted_features->SIM_code, "READY")) {
		return SIM_READY;
	}
	if (!strcmp(sim80x->extracted_features->SIM_code, "SIM PIN")) {
		return SIM_PIN;
	}
	if (!strcmp(sim80x->extracted_features->SIM_code, "SIM PUK")) {
		return SIM_PUK;
	}
	if (!strcmp(sim80x->extracted_features->SIM_code, "PH_SIM PIN")) {
		return PH_SIM_PIN;
	}
	if (!strcmp(sim80x->extracted_features->SIM_code, "PH_SIM PUK")) {
		return PH_SIM_PUK;
	}
	if (!strcmp(sim80x->extracted_features->SIM_code, "SIM PIN2")) {
		return SIM_PIN2;
	}
	if (!strcmp(sim80x->extracted_features->SIM_code, "SIM PUK2")) {
		return SIM_PUK2;
	}
	return SIM_PIN_ERROR;
}


/*!
* \brief
*	Searches the received data for the DNS and PORT features.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\param  select, set this 0 for DNS only or to 1 for both DNS and port.
*\return sim80x->extracted_features.DNSandPORT:		a char array, pointing to the DNSandPORT array inside the extracted_features structure, inside the sim80x type structure.
*/
static char* _Sim80x_extract_DNS (sim80x_t* sim80x) {

	char* start, *end, *temp_end;
	char delimit[5] = "www.";
	char temp_received[GSM_UART_BUFFER_SIZE];
	for (int i=0; i<GSM_UART_BUFFER_SIZE; ++i) {
		temp_received[i] = receivedDataBuffer_fromSim80x[i];
	}
	start = &temp_received[0];
	end = strstr(temp_received, delimit); // The end points to the (first) <w>ww.

	int index = end - start; // The index indicates the position of the (first) <w>ww. in the received data stream.
	sim80x->extracted_features->DNS[0] = receivedDataBuffer_fromSim80x[index];
	sim80x->extracted_features->DNS[1] = receivedDataBuffer_fromSim80x[index + 1];
	sim80x->extracted_features->DNS[2] = receivedDataBuffer_fromSim80x[index + 2];
	sim80x->extracted_features->DNS[3] = receivedDataBuffer_fromSim80x[index + 3];

	// Now the DNS has www.

	index += 4;
	// Now index points to www.<>...
	delimit[0] = '\"';
	delimit[1] = delimit[2] = delimit[3] = delimit[4] = '\0';

	temp_end = &end[index]; // temp_end has www.<s>ite.com"...
	end = strtok(temp_end, delimit);
	int dns_length = strlen(end); // Length of DNS substring. I.e. for ..."www.google.com",... -> google.com",...
	int i;
	for (i=0; i<dns_length; ++i) {
		sim80x->extracted_features->DNS[i+4] = receivedDataBuffer_fromSim80x[i+index];
	}

	i = 4 + dns_length;
	index = index + dns_length;

	// Now these pointers point to the next position for future additions...
	return sim80x->extracted_features->DNS;
}




/*!
 *\brief
 *	Searches the received data for the TCP or UDP feature.
 *\return a protocol_en type variable, informing us whether the protocol mode is TCP or UDP, using the enumerator type protocol_en defined in the header file.
 */

static protocol_en _Sim80x_extract_protocol_mode (void) { // Check whether the connection mode is TCP or UDP.
	const char check_TCP[10] = {"TCP"};
	const char check_UDP[10] = {"UDP"};

	if ((strstr(check_receivedDataBuffer_fromSim80x, check_TCP) != NULL)) {
		return TCP;
	}
	if ((strstr(check_receivedDataBuffer_fromSim80x, check_UDP) != NULL)) {
		return UDP;
	}
	return TCP;
}



/*!
* \brief
*	Searches the received data for the QoS feature.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\return a QoS_en type variable, informing us about the signal strength characterization, using the enumerator type QoS_en defined in the header file.
*/

static QoS_en _Sim80x_extract_QoS(sim80x_t *sim80x) {
//	char temp_QoS[5];
	int db;

//	for (int i=0; i < 5; ++i) {
//		temp_QoS[i] = 0;
//	}
//	char temp_received[50]; // The QoS response is of the form: +CSQ: rssi,ber
	char *temp;
//	for (int i=0; i<50; ++i) {
//		temp_received[i] = receivedDataBuffer_fromSim80x[i]; // We save only the rssi,ber and reject the "+CSQ: "
//	}

	if ((temp = strstr(receivedDataBuffer_fromSim80x, "+CSQ: ")) == NULL)
		return UNKNOWN;

	sscanf (temp, "+CSQ: %d,", &db);
//	const char delimit[2] = ",";
//	char *token;
//	token = strtok(temp_received, delimit);
//	int QoS_length = strlen(token);
//	for (int i=0; i < QoS_length; ++i) {
//		temp_QoS[i] = token[i];
//	}
//
//	// QoS[0-1] = "16" (i.e.)
//	// QoS[2-...] = 0
//	int db;
//	db = 10*(temp_QoS[0] - '0') + (temp_QoS[1] - '0'); // The decimal equal of the character number.
	sim80x->extracted_features->QoS = db;
	if (db < 2) {
		return BAD;
	}
	else if (db < 10) {
		return MARGINAL;
	}
	else if ( db < 15) {
		return GOOD;
	}
	else if (db < 20) {
		return HIGH;
	}
	else if (db < 31) {
		return EXCELLENT;
	}
	else if ((db == 31) && (db != 99)) {
		return SUPER_EXCELLENT;
	}
	return UNKNOWN;
}

/*!
* \brief
*	Searches the received data for the QoS feature.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\return a QoS_en type variable, informing us about the signal strength characterization, using the enumerator type QoS_en defined in the header file.
*/

static handle_status_en _Sim80x_extract_Location(sim80x_t *sim80x) {

	int code;
	char *temp;

	if ((temp = strstr(receivedDataBuffer_fromSim80x, "+CIPGSMLOC: ")) == NULL)
		return SIM80_SIM_ERROR;

	sscanf (temp, "+CIPGSMLOC: %d,", &code);
	if (code == 0) {
		sscanf (temp, "+CIPGSMLOC: %d,%f,%f,", &code, &sim80x->extracted_features->location.longitude, \
													  &sim80x->extracted_features->location.latitude);
	}

	return SIM80_OK;
}






/*!
* \brief
*	Searches the received data for the BER feature.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type,
*		 defined in the header file
*\return a BER_en type variable, informing us about the signal bit error rate
*		 characterization, using the enumerator type BER_en defined in the header file.
*/

static BER_en _Sim80x_extract_BER(sim80x_t *sim80x) {
	char temp_BER[5];

	for (int i=0; i < 5; ++i) {
		temp_BER[i] = 0;
	}
	char temp_received[20]; // The QoS response is of the form: +CSQ: rssi,ber
	for (int i=0; i<20; ++i) {
		temp_received[i] = receivedDataBuffer_fromSim80x[i+6]; // We save only the rssi,ber and reject the "+CSQ: "
	}
	const char delimit[2] = ",";
	char *token;
	token = strtok(temp_received, delimit);
	while( token != NULL ) {
		token = strtok(NULL, delimit); // The token pointer now points to the BER digit (0-7).
	}
	int BER_length = strlen(token); // Here this is always 1 byte = 1 character.
	for (int i=0; i < BER_length; ++i) {
		temp_BER[i] = token[i];
	}
	int bit_error_rate;
	bit_error_rate = temp_BER[0] - '0';	// The decimal equal of the character number.
	sim80x->extracted_features->BER = bit_error_rate;
	if (bit_error_rate < 1) {
		return EXTRA_LOW_ERROR_RATE;
	}
	else if (bit_error_rate < 4) {
		return LOW_ERROR_RATE;
	}
	else if (bit_error_rate < 6) {
		return HIGH_ERROR_RATE;
	}
	return EXTRA_HIGH_ERROR_RATE;
}



/*!
* \brief
*	Searches the received data for the DNS and IP features.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\param  select, set this 0 for DNS only or to 1 for both DNS and IP, 2 only for ip.
*\return sim80x->extracted_features.DNSandIP:	a char array, pointing to the DNSandIP array inside the extracted_features structure, inside the sim80x type structure.
*/

static char *_Sim80x_extract_DNSandIP(sim80x_t *sim80x, int select) {// Set select to 0 for DNS only or to 1 for both DNS and IPs (all the available ones).
															 // If set to 2, we return only the IP1 and if set to 3, only the IP2 if exists.
															 // The user knows the command that he sent, so he expects a respective response.
															 // Thus, i.e., if he is expecting for an IP to be returned as response, then he
															 // will call the getIP().
	char *start, *end, *end2, *temp_end;
	char delimit[5] = "www.";
	char temp_received[256], temp_received2[256];
	for (int i=0; i<256; ++i) {
		temp_received[i] = receivedDataBuffer_fromSim80x[i];
		temp_received2[i] = receivedDataBuffer_fromSim80x[i];
	}
	start = &temp_received[0];
	end = strstr(temp_received, delimit); // The end points to the (first) <w>ww.

	int index = end - start; // The end pointer indicates the first character after the first 'www.' delimiter string in the DNS.
	sim80x->extracted_features->DNSandIP[0] = receivedDataBuffer_fromSim80x[index];
	sim80x->extracted_features->DNS[0] = sim80x->extracted_features->DNSandIP[0];

	sim80x->extracted_features->DNSandIP[1] = receivedDataBuffer_fromSim80x[index + 1];
	sim80x->extracted_features->DNS[1] = sim80x->extracted_features->DNSandIP[1];

	sim80x->extracted_features->DNSandIP[2] = receivedDataBuffer_fromSim80x[index + 2];
	sim80x->extracted_features->DNS[2] = sim80x->extracted_features->DNSandIP[2];

	sim80x->extracted_features->DNSandIP[3] = receivedDataBuffer_fromSim80x[index + 3];
	sim80x->extracted_features->DNS[3] = sim80x->extracted_features->DNSandIP[3];

	// Now the DNSandIP has www.

	index += 4;
	// Now index points to www.<>...
	delimit[0] = '\"';
	delimit[1] = delimit[2] = delimit[3] = delimit[4] = '\0';

	temp_end = &temp_received[index]; // temp_end has www.<s>ite.com"...
	end = strtok(temp_end, delimit);

	int dns_length = strlen(end); // Length of DNS substring. I.e. for ..."www.google.com",... -> google.com",...
	int i;
	for (i=0; i<dns_length; ++i) {
		sim80x->extracted_features->DNSandIP[i+4] = receivedDataBuffer_fromSim80x[i+index];
		sim80x->extracted_features->DNS[i+4] = sim80x->extracted_features->DNSandIP[i+4];
	}

	i = 4 + dns_length;
	sim80x->extracted_features->DNS[i] = '\0';
	if (!select) {
		return sim80x->extracted_features->DNS;
	}



	index = index + dns_length;
	sim80x->extracted_features->DNSandIP[i] = ','; // At position <4 + dns_length>.
	index++; // Now the index points here: "www.google.com","255.255.255.255" -> "<,>"255.255.255.255"...
	index++; // Now the index points here: "www.google.com","255.255.255.255" -> ",<">255.255.255.255"...
	index++; // Now the index points here: "www.google.com","255.255.255.255" -> ","<2>55.255.255.255"...
	//end = &temp_received[index]; // Now we point to the first digit of the (first) IP address.

	end2 = &temp_received2[index];
	end2 = strtok(end2, delimit);	// end2 -> ..."<2>55.255.255.255","255.255.255.255"

	++i;	// i points to the DNSandIP position after the ,
	int j;
	int IP1_length = strlen(end2);
	for (j=0; j<IP1_length; j++) {
		sim80x->extracted_features->DNSandIP[i+j] = end2[j];
		sim80x->extracted_features->IP1[j] = end2[j];
	}

	sim80x->extracted_features->IP1[j] = '\0';

	if (select==2) {
		sim80x->extracted_features->DNSandIP[i+j] = '\0';
		return sim80x->extracted_features->IP1;
	}

	i += IP1_length;

	sim80x->extracted_features->DNSandIP[i] = ',';

	// i indicates the position for DNSandIP and index indicates the position of the input buffer, temp_received2.
	++i;
	index += IP1_length;

	end2 = &temp_received2[index];
	end2 += 2;	// end2 points to ..."255.255.255.255",<">255.255.255.255" if exists
	if (end2[0] == '\"') {
		end2++;
		// end2 points to the first digit of the second IP (if exists, otherwise is NULL) -> ..."<2>55.255.255.255"
		int IP2_length = strlen(end2);
		IP2_length--;
		for (j=0; j<IP2_length; j++) {
			sim80x->extracted_features->DNSandIP[i+j] = end2[j];
			sim80x->extracted_features->IP2[j] = end2[j];
		}
		sim80x->extracted_features->DNSandIP[i+j] = '\0';
		sim80x->extracted_features->IP2[j] = '\0';
	}

	if (select==3) {
		return sim80x->extracted_features->IP2;
	}

	return sim80x->extracted_features->DNSandIP;
}



/*!
* \brief
*	Searches the received data for the send data length feature.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\return digit:		an integer with the length, which is defined inside the extracted_features structure, inside the sim80x type structure.
*/

static unsigned int _Sim80x_extract_length (sim80x_t* sim80x) {

	char temp_received[30]; // The response is of the form: +<command>: <length>...
	unsigned int digit = 0;

	for (int i=0; i<30; ++i)
		temp_received[i] = receivedDataBuffer_fromSim80x[i];

	char delimit[2] = ":";
	char* token;
	token = strtok(temp_received, delimit); // The first token is +<command>
	while( token != NULL ) {
		token = strtok(NULL, delimit);		// The second token is <length>...
		break;
	}

//	strcpy(delimit, "\n");
//	while( token != NULL ) {
//		token = strtok(NULL, delimit);		// The third token is <length>
//		break;
//	}

/*
 	int length = strlen(token);

	for (int i=1; i<=length; ++i) {
		digit = digit + (token[i] - '0')*pow(10,(length - i));
		// I.e.: for 32723 -> d = 0 + 3*10^4 + 2*10^3 + 7*10^2 + 2*10^1 + 3*10^0 = 30000 + 2000 + 700 + 20 + 3 = 32000 + 723 = 32723.
	}
*/
	sscanf(token, " %d", &digit);
	sim80x->extracted_features->send_length = digit;

	return digit;

}


/*!
* \brief
*	Searches the received data for the send data length feature.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\return digit:		an integer with the length, which is defined inside the extracted_features structure, inside the sim80x type structure.
*/

static unsigned int _Sim80x_extract_err_length (void) {
	char temp_received[30]; // The response is of the form: +<command>: <length>...
	for (int i=0; i<30; ++i) {
		temp_received[i] = receivedDataBuffer_fromSim80x[i];
	}
	char delimit[2] = ":";
	char* token;
	token = strtok(temp_received, delimit); // The first token is +<command>
	while( token != NULL ) {
		token = strtok(NULL, delimit);		// The second token is <length>...
		break;
	}

	int digit = 0;
	sscanf (token, " %d", &digit);

	/* This code can not be used due to speed limitations */
//	int length = strlen(token) - 1;
//
//	int digit = 0;
//	for (int i=0; i<length; ++i) {
//		digit = digit + (token[i+1] - '0')*pow(10,(length-1 - i));
//		// I.e.: for 32723 -> d = 0 + 3*10^4 + 2*10^3 + 7*10^2 + 2*10^1 + 3*10^0 = 30000 + 2000 + 700 + 20 + 3 = 32000 + 723 = 32723.
//	}
	return digit;
}


/*!
* \brief
*	Searches the received data for the transmitted data length, acknowledged and not-acknowledged by the server data length features.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\return a handle_status_en type variable, using the enumerator type handle_status_en defined in the header file.
*/

static handle_status_en _Sim80x_extract_ack_status (sim80x_t* sim80x) {

	int tx_digit = 0;
	int ack_digit = 0;
	int nack_digit = 0;

	char temp_received[30]; // The CIPACK response is of the form: +CIPACK: <txlen>, <acklen>, <nacklen>
	char* token;

	const char delimit[2] = ",";

	for (int i=0; i<30; ++i) {
		temp_received[i] = receivedDataBuffer_fromSim80x[i+9]; // We save only the length and reject the "+CIPACK: "
	}
	token = strtok(temp_received, delimit);

//	int tx_length = strlen(token);
//	for (i=0; i<tx_length; ++i) {
//		tx_digit = tx_digit + (token[i] - '0')*pow(10,(tx_length-1 - i));
//		// I.e.: for 32723 -> d = 0 + 3*10^4 + 2*10^3 + 7*10^2 + 2*10^1 + 3*10^0 = 30000 + 2000 + 700 + 20 + 3 = 32000 + 723 = 32723.
//	}
	sscanf (token, "%d", &tx_digit);
	sim80x->extracted_features->txlen = tx_digit;

	while( token != NULL ) {
		token = strtok(NULL, delimit);
		break;
	}
//	int ack_length = strlen(token);
//	for (i=1; i<ack_length; ++i) {
//		ack_digit = ack_digit + (token[i] - '0')*pow(10,(ack_length-1 - i));
//		// I.e.: for 32723 -> d = 0 + 3*10^4 + 2*10^3 + 7*10^2 + 2*10^1 + 3*10^0 = 30000 + 2000 + 700 + 20 + 3 = 32000 + 723 = 32723.
//	}
	sscanf (token, "%d", &ack_digit);
	sim80x->extracted_features->acklen = ack_digit;

	while( token != NULL ) {
		token = strtok(NULL, delimit);
		break;
	}
//	int nack_length = strlen(token);
//	for (i=0; i<nack_length; ++i) {
//		nack_digit = nack_digit + (token[i] - '0')*pow(10,(nack_length-1 - i));
//		// I.e.: for 32723 -> d = 0 + 3*10^4 + 2*10^3 + 7*10^2 + 2*10^1 + 3*10^0 = 30000 + 2000 + 700 + 20 + 3 = 32000 + 723 = 32723.
//	}
	sscanf (token, "%d", &nack_digit);
	sim80x->extracted_features->nacklen = nack_digit;
	if (nack_digit > ack_digit)
		return SIM80_SIM_ERROR;
	return SIM80_OK;
}


/*!
* \brief
*	Searches the received data for the registration result codes and status features.
*\param  *sim80x, a sim80x_t type pointer of the respective structure type, defined in the header file
*\return a registration_en type variable, using the enumerator type registration_en defined in the header file.
*/

static registration_en _Sim80x_extract_registration (sim80x_t* sim80x) {
	char temp_received[30];		// CREG response of the form: +CREG: <n>,<stat>,"<lac>","<ci>"
	for (int i=0; i<30; ++i) {
		temp_received[i] = receivedDataBuffer_fromSim80x[i+7]; // We save only the length and reject the "+CREG: "
	}
	int index = 0;
	int n = temp_received[index] - '0';
	index += 2; // The index points to the ',' between the first two numbers.

	int status = temp_received[index] - '0';
	sim80x->extracted_features->reg_code = n;
	sim80x->extracted_features->reg_status = status;
//	if (n>0) {
		if (status==0)
			return UNREGISTERED;
		else if (status==1)
			return REGISTERED;
		else if (status == 2)
			return SEARCHING;
		else if (status == 3)
			return DENIED;
		else if (status == 4)
			return UNKNOWN_NETWORK;
		else if (status == 5)
			return ROAMING;
//	}
	return DISABLED_NETWORK;
}






handle_status_en Sim80x_Sapbr (sim80x_t* sim80x, char* param, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_GPRS_bearer_sets, param);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}





/*  ================================================================
 *	=============		PUBLIC SIM80X FUNCTIONS		  ==============
 *	================================================================
 */

/*  ================================================================
 *		Test Function for establishing a TCP Connection with a
 *					 server with an IP or a DNS
 *	================================================================
 */

void Sim80x_test(void)
{
	sim80x_t Sim80x;
	extracted_features_t extracted_features;
	GPRS_t GPRS;


	Sim80x_link_CheckReceive(&Sim80x, GSM_CheckReceive);
	Sim80x_link_ReceiveFlush(&Sim80x, GSM_ReceiveFlush);
	Sim80x_link_getchar(&Sim80x, GSM_getchar);
	Sim80x_link_putchar(&Sim80x, GSM_putchar);


//	QoS_en qstatus;
//	BER_en bstatus;
	handle_status_en hstatus;
	attach_detach_en astatus;
//	registration_en rstatus;

	float input_timeout = 2;
//	Sim80x_test_recv_Data(&Sim80x, "", input_timeout);

	int mode = 0;
//	uint32_t i=0;
	uint32_t diffussion = 100;
	clock_t now, total;
	//if (_done == 0) {
		uint32_t timeout = 1;
		total = timeout*get_freq();


		now = clock();
		GSM_PWR(1);
		while (_CLOCK_DIFF (clock (), now) <= 0.5*total)
			;

		GSM_ON(1);
		while (_CLOCK_DIFF (clock (), now) <= total)
			;

		GSM_ON(0);
		while (_CLOCK_DIFF (clock (), now) <= total)
			;

		GSM_UART_Init();

		now = clock();
//		GSM_PWR(0);

		while(_CLOCK_DIFF (clock (), now) <= total)
			;

		GSM_PWR(1);
		GSM_ON(1);

		Sim80x_set_PIN(&Sim80x, "5064");
		Sim80x_set_baud_rate(&Sim80x, "115200");
		_done = 1;


		/* If the user wants to call for some reason the Sim80x_Init() function again
		 * the mode is mandatory.
		 * 	0 if the function is called for the first time
		 * 	1 otherwise
		 */
		if ((hstatus = Sim80x_Init (&Sim80x, &extracted_features, &GPRS, mode, input_timeout)) != SIM80_OK)
			hstatus = SIM80_SIM_FUN_ERROR;

//		Sim80x_get_registration(&Sim80x, input_timeout, &rstatus);

//		now = clock();
//		while (_CLOCK_DIFF (clock (), now) <= diffussion)
//			;
//		rstatus = DISABLED_NETWORK;
//		qstatus = BAD;
//		now = clock();
//		while (_CLOCK_DIFF (clock (), now) <= 2*diffussion)
//			;
//		do {
//			Sim80x_get_registration(&Sim80x, input_timeout, &rstatus);
//			now = clock();
//			while (_CLOCK_DIFF (clock (), now) <= 2*diffussion)
//				;
//
//			GPRS_check_signal(&Sim80x, &qstatus, &bstatus, input_timeout);
//			now = clock();
//			while (_CLOCK_DIFF (clock (), now) <= 2*diffussion)
//				;
//		} while (rstatus == DISABLED_NETWORK || qstatus == BAD) ;


//	}

//	GPRS_get_netlight_status(&Sim80x, &rstatus, input_timeout);
//	while (rstatus != ENABLED_NETWORK) {
//		now = clock();
//		while (_CLOCK_DIFF (clock (), now) <= diffussion)
//			;
//		GPRS_get_netlight_status(&Sim80x, &rstatus, input_timeout);
//	}

	char apn_in[] = "internet.vodafone.gr";
	GPRS_set_APN(&Sim80x, apn_in);

	char protocol[] = "TCP";
	GPRS_set_protocol(&Sim80x, protocol);

	char dns_in[] = "94.69.222.190";
	GPRS_set_Domain(&Sim80x, dns_in);


	char port[] = "50000";
	GPRS_set_port(&Sim80x, port);


	if (GPRS_Init (&Sim80x, &astatus) != SIM80_OK)
		hstatus = SIM80_SIM_FUN_ERROR;

//	while (astatus != ATTACHED) {
//
//		GPRS_Init (&Sim80x, _hash_table, &astatus, &attempts_num, input_timeout);
//		now = clock();
//		while (_CLOCK_DIFF (clock (), now) <= diffussion)
//			;
//
//		GPRS_get_netlight_status(&Sim80x, &rstatus, input_timeout);
//		now = clock();
//		while (_CLOCK_DIFF (clock (), now) <= diffussion)
//			;
//	}
//	while (astatus != ATTACHED) {
//	GPRS_Init (&Sim80x, _hash_table, &astatus, &attempts_num, input_timeout);
//		if(++i >= attempts_num)
//			break;
//	}
//	clock_t now;

	now = clock();
	while (_CLOCK_DIFF (clock (), now) <= diffussion)
		;

	if (Sim80x.io.CheckReceive() != 0)
		Sim80x.io.ReceiveFlush();

	GPRS_get_local_ip(&Sim80x, 10);
	while (!((Sim80x.extracted_features->localIP[0] >= 48) && 	 \
		   (Sim80x.extracted_features->localIP[0] <= 57)))
	{
		now = clock();
		while (_CLOCK_DIFF (clock (), now) <= diffussion)
			;
		GPRS_get_local_ip(&Sim80x, 10);					// x.x.x.x
	}

//	i=0;
//	char* server = "\"TCP\",\"www.facebook.com\",50000";
//	while (GPRS_start_TCP_UDP_DNS_con(&Sim80x, _hash_table, server, input_timeout) != SIM80_CONNECT_OK) {
//		if (++i >= attempts_num)
//			break;
//	}

//	i=0;
	now = clock();
	while (_CLOCK_DIFF (clock (), now) <= diffussion)
		;
//	strcpy(receivedDataBuffer_fromSim80x, "+CDNSGIP: 1,\"www.facebook.com\",\"123.456.789.255\"");
	if (Sim80x.io.CheckReceive() != 0)
		Sim80x.io.ReceiveFlush();

	uint32_t ff=0;

	// If test_GPRS_DNS_IP_query is called with param = NULL, then the function will query the IP of the sim dns
	// inserted by the use. Otherwise, the function will query the IP of the param variable.
	char param[] = "www.facebook.com";
	while(1) {
		++ff;
		if ((GPRS_DNS_IP_query(&Sim80x, param, input_timeout)) == SIM80_OK)
			break;
//		_GPRS_DNS_IP_tx_rx_query(&Sim80x, param, 10); // edit this for secondary IP
//		ff++;
//		if (receivedDataBuffer_fromSim80x[6] == '+') {
////			while (receivedDataBuffer_fromSim80x[6] != '+') {
////				now = clock();
////				while (_CLOCK_DIFF (clock (), now) <= diffussion)
////					;
////				test_GPRS_DNS_IP_query(&Sim80x, "\"www.facebook.com\"", 10);
////			}
//
//			//_Sim80x_extract_DNSandIP(sim80x, 2);
//			if (receivedDataBuffer_fromSim80x[16] == '1') {
//				_Sim80x_extract_DNSandIP(&Sim80x, 2);
//				hstatus = SIM80_OK;
//				break;
//
//			}
//			else if (receivedDataBuffer_fromSim80x[16] == '0') {
//				hstatus = DNS_ERROR;
//				continue;
//			}
//
//		}
//		else if (receivedDataBuffer_fromSim80x[0] == '+') {
////			while (receivedDataBuffer_fromSim80x[0] != '+') {
////				now = clock();
////				while (_CLOCK_DIFF (clock (), now) <= diffussion)
////					;
////				test_GPRS_DNS_IP_query(&Sim80x, "\"www.facebook.com\"", 10);
////			}
//				//_Sim80x_extract_DNSandIP(sim80x, 2);
//			if (receivedDataBuffer_fromSim80x[10] == '1') {
//				_Sim80x_extract_DNSandIP(&Sim80x, 2);
//				hstatus = SIM80_OK;
//				break;
//			}
//			else if (receivedDataBuffer_fromSim80x[10] == '0') {
//				hstatus = DNS_ERROR;
//				continue;
//			}
//		}
	}


/*
 * =================ESTABLISH TCP OR UDP CONNECTION======================
 */

/*!
 * \brief
 * 	  Until now, the connection over GPRS with the SIM card is initialized.
 * 	  From now on, with the AT commands <AT+CIPSTART> and the <AT+CIPSEND>
 * 	  begins the process of sending the data to a remote server.
 *
 * 	  In the function GPRS_start_TCP_UDP_DNS_con enter as a string
 * 	  the mode to operate ("TCP" or "UDP"), the IP or the DNS domain name
 * 	  and the port from which the connection is established (e.g. "50000")
 */

	/* Since there is a chance that the server is down we do not want the program to freeze.
	 * Thus, the SIM80x module is going to attempt some times to establish connection but not
	 * constantly. After the total number of attempts that failed, the module will stop
	 * putting effort in the communication.
	 */
//	uint32_t i=0;



	if (Sim80x.io.CheckReceive() != 0)
		Sim80x.io.ReceiveFlush();

	clock_t now2;
	now = clock();
	hstatus = GPRS_start_TCP_UDP_DNS_con(&Sim80x, input_timeout);
	while (hstatus != SIM80_CONNECT_OK  &&  hstatus!= SIM80_ALREADY_CONNECTED) {
		if (_CLOCK_DIFF(clock(), now) >= CONNECT_TIMEOUT*1000) {
			hstatus = SIM80_TIMEOUT;
			break;
		}
		now2 = clock();
		while (_CLOCK_DIFF(clock(), now2) <= 500)
			;
		hstatus = GPRS_start_TCP_UDP_DNS_con(&Sim80x, input_timeout);
	}



/*  ================================================================
 *		IP HEADER Before Data:  \r\n+IPD,<size>:<payload>
 *	================================================================
 */
/*!
 * \brief
 * 	  In order to allocate the exact amount of memory required for the
 * 	  received data, the AT+CIPHEAD command is used. The Header of the
 * 	  received data is of the format: +IPD,size:payload---> Be careful! NO SPACES
 */
	//AT+CIPHEAD
	hstatus = GPRS_ip_head(&Sim80x, "1", input_timeout);
	int i=0;
//	int temp_flag = 1;
	while (hstatus != SIM80_OK) {
		if (i++ >= ATTEMPTS_NUM)
			hstatus = SIM80_SIM_FUN_ERROR;
		hstatus = GPRS_ip_head(&Sim80x, "1", input_timeout);
	}


	/* Now it is time to receive the data that the server sends */
	char getData[20];
	GPRS_getData(&Sim80x, getData, 9, 10);

//	now = clock();
//	hstatus = Sim80x_set_timestamp(&Sim80x, input_timeout);
//	now = clock();
//	while (hstatus != SIM80_OK)
//		hstatus = Sim80x_set_timestamp(&Sim80x, input_timeout);
//
//
//	struct tm timestamp;
//	now = clock();
//	hstatus = Sim80x_get_timestamp(&Sim80x, &timestamp, input_timeout);
//	while (hstatus != SIM80_OK) {
//		hstatus = Sim80x_get_timestamp(&Sim80x, &timestamp, input_timeout);
//		if (hstatus == SIM80_OK)
//			break;
//	}

///*
// *==============================SEND DATA TO REMOTE SERVER==============================
// */
//
///*!
// * \brief
// *    Here the AT+CIPSEND=<length> command is executed. Using this command with
// *    the parameter of the length to send, the prerequisite for a delimeter character,
// *    the 0x26 or 0x1A or CTRL+Z is eliminated.
// */
//	char* data = "1010101030443865825694258245865438256652655641";
	int array[] = {65,66,67,68,69,70,71,72,47,48,55,64,32,21,22,23,24,25,0};

//	char array[1025];
	unsigned int size = sizeof(array);
	GPRS_send_data(&Sim80x, array, size, 10);


i = 0;
if(i) {

	now = clock();
	char* ntp_server = "time.google.com";
//	hstatus = Gen_set_timestamp(&Sim80x, ntp_server, input_timeout);
	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Gen_set_timestamp(&Sim80x, ntp_server, input_timeout)) == SIM80_OK)
			break;
	}

	struct tm timestamp;
	now = clock();
//	hstatus = Gen_get_timestamp(&Sim80x, &timestamp, input_timeout);
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Gen_get_timestamp(&Sim80x, &timestamp, input_timeout)) == SIM80_OK)
			break;
	}

	now = clock();
//	hstatus = Gen_Loc_and_Date(&Sim80x, "1,1", input_timeout);
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Gen_Loc_and_Date(&Sim80x, "1,1", input_timeout)) == SIM80_OK)
			break;
	}
	//Gen_Loc_and_Date(&Sim80x, "1,1", input_timeout);
}


	now = clock();
	while ((GPRS_TCP_close(&Sim80x, input_timeout)) != SIM80_CLOSE_OK) {
		if(_CLOCK_DIFF(clock(), now) >= CLOSE_TIMEOUT*1000) {
			hstatus = SIM80_TIMEOUT;
			break;
		}
	}

	now = clock();
	while ((GPRS_PDP_close(&Sim80x, input_timeout)) != SIM80_SHUT_OK) {
		if(_CLOCK_DIFF(clock(), now) >= CLOSE_TIMEOUT*1000) {
			hstatus = SIM80_TIMEOUT;
			break;
		}
	}



}


/* Link functions with function pointers that connect the specific implementation with the custom hardware */
void Sim80x_link_putchar (sim80x_t *sim80x, Sim80x_putchar_ft fun)		 	 	 {sim80x->io.Putchar   		= fun; }
void Sim80x_link_getchar (sim80x_t *sim80x, Sim80x_getchar_ft fun)   		 	 {sim80x->io.Getchar        = fun; }
void Sim80x_link_CheckReceive (sim80x_t *sim80x, Sim80x_CheckReceive_ft fun) 	 {sim80x->io.CheckReceive   = fun; }
void Sim80x_link_ReceiveFlush (sim80x_t *sim80x, Sim80x_ReceiveFlush_ft fun)     {sim80x->io.ReceiveFlush	= fun; }


/*!
* \brief
*	The Initialization function of the SIM80x module.
*	With this function the initialization commands to the SIM80x
*	module are sent.
*/
handle_status_en Sim80x_Init (sim80x_t* sim80x, extracted_features_t *extracted_features, \
							   GPRS_t *GPRS, int mode, float input_timeout) {
	// Set the initial parameters manually...
	handle_status_en hstatus;
	Sim80x_flag = 0;

/*  Not used in the current implementation
 *		registration_en rstatus;
 *		SIM_codes_en sstatus;
 */

	/* Not required to be defined by the user. After experiments (trial and error) The most suitable
	 * value for the number of repetitions (ATTEMPTS_NUM) is 100
	 */
	clock_t now, mark, sim_now;
	uint8_t temp_flag;

/* =========================================================================
 * ==========    sim80x initialize features inside the struct     ==========
 * =========================================================================
 */
	sim80x->AT = (AT_t*)&AT_st;
	sim80x->responses = (responses_t*)&rsp_st;
	sim80x->SIM_Equipment_Error_msgs = (SIM_Equipment_Error_msgs_t*)&SIM_Equipment_Error_msgs_st;
	sim80x->error_numbers = _error_numbers;
	sim80x->GPRS = GPRS;
	sim80x->extracted_features = extracted_features;

	/* Create the hash map that matches each error number to the appropriate error message */
	for (int z=0; z<HASH_SIZE; ++z) {
		if (_hash_insert(_hash_table, sim80x->SIM_Equipment_Error_msgs->number[z], sim80x->error_numbers[z]) == HASH_ERROR)
			break;
	}

//	char test_str[] = "RDY";//\r\n\r\n+CFUN: 1\r\n";
//	char temp[2*GSM_UART_BUFFER_SIZE];		//Reserve an array of 2*GSM_UART_BUFFER_SIZE to find the init Series.


/* ============================================================
 * ==========   Init Sequence of the Sim80x Module   ==========
 * ============================================================
 */
#ifdef DEBUG
	while (1) {
		temp[i++] = (char)sim80x->io.Getchar();
		if (strstr (temp, test_str)) {
			break;
		}
		if (i == (sizeof (temp) - 1))
			i = 0;
	}

#else
//	temp_flag = 1;
//	now = clock();
//	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT*1000) {
//		if (sim80x->io.CheckReceive())
//			temp[i++] = (char)sim80x->io.Getchar();
//		if (strstr (temp, test_str)) {
//			temp_flag = 0;
//			break;
//		}
//		if (i == (sizeof (temp) - 1))
//			i = 0;
//	}
//
//	if (temp_flag)
//		return SIM80_SIM_FUN_ERROR;

#endif
/* ======================================
 * ==========        AT        ==========
 * ======================================
 */
//	if (sim80x->io.CheckReceive() != 0)
//		sim80x->io.ReceiveFlush();

#ifdef DEBUG
	while ((hstatus = Sim80x_set_AT (sim80x, mode, input_timeout)) != SIM80_OK)
		;

#else
	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= 4000)
		;
	temp_flag = 1;
	mark = clock ();
	do {
      if ((hstatus = Sim80x_set_AT (sim80x, mode, input_timeout)) == SIM80_OK) {
         temp_flag = 0;
         break;
      }
      now = clock ();
	} while (_CLOCK_DIFF(now, mark) <= GENERAL_TIMEOUT * get_freq());

	/* Protection over the fact of never finding the correct initialization sequence */
	if (temp_flag)
		return SIM80_SIM_FUN_ERROR;

#endif

/* ======================================
 * ==========     AT+IPR=x     ==========
 * ======================================
 */
	if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

#ifdef DEBUG
	while ((hstatus = Sim80x_insert_baud_rate (sim80x, mode, input_timeout)) != SIM80_OK) {
		;
	}

#else
	temp_flag = 1;
	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT * get_freq()) {
		if ((hstatus = Sim80x_insert_baud_rate (sim80x, mode, input_timeout)) == SIM80_OK) {
			temp_flag=0;
			break;
		}
	}
	if (temp_flag)
		return SIM80_SIM_FUN_ERROR;

#endif


/* ======================================
 * ==========      ATE0        ==========
 * ======================================
 */
	if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

#ifdef DEBUG
	/* Infinite Loop to verify success of the command. This is not required for normal mode */
	while (hstatus != SIM80_OK)
		hstatus = Sim80x_set_echo_mode (sim80x, input_timeout);

#else
	temp_flag = 1;
	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Sim80x_set_echo_mode (sim80x, input_timeout)) == SIM80_OK) {
			temp_flag = 0;
			break;
		}
	}
	/* Protection over the fact of never finding the correct initialization sequence */
	if (temp_flag)
		return SIM80_SIM_FUN_ERROR;

#endif


//	//AT+CMEE=1, Report Mobile Equipment Error
//	char* err_param = "1";
	if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

#ifdef DEBUG
	while (1) {
		hstatus = Sim80x_error_report (sim80x, "1", input_timeout);
		if (hstatus == SIM80_OK)
			break;
	}

#else
	temp_flag = 1;
	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Sim80x_error_report (sim80x, "1", input_timeout)) == SIM80_OK) {
			temp_flag = 0;
			break;
		}
	}

	/* Protection over the fact of never finding the correct initialization sequence */
	if (temp_flag)
		return SIM80_SIM_FUN_ERROR;
#endif


	//AT+CFUN=1, set full functionality
	//The value is by default 1, so there is no actual need in calling this AT command
	hstatus = Sim80x_set_functionality (sim80x, "1", input_timeout);

	//AT+CPIN="xxxx,"yyyy", PIN will be inserted by main
	//It is important to receive a positive answer from the SIM80x module before we initiate the
	//complete function of the system

	if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

#ifdef DEBUG
	i=0;
	while((hstatus = Sim80x_insert_PIN(sim80x, _hash_table, SIM_PIN_TIMEOUT)) != SIM80_OK) {

		now = clock();
		while (_CLOCK_DIFF (clock (), now) <= 100)
			;
		++l;		//This variable should be used only for DEBUG

	}
#else
	sim_now = clock();
	while (_CLOCK_DIFF(clock(), sim_now) <= 1000*3*SIM_PIN_TIMEOUT) {			/* Timeout after 15 seconds --> 3*5= 15s */
		hstatus = SIM80_SIM_ERROR;
		if ((hstatus = Sim80x_insert_PIN(sim80x, SIM_PIN_TIMEOUT)) == SIM80_OK)
			break;
		else {
			now = clock();
			while (_CLOCK_DIFF (clock (), now) <= 100)
				;
		}
	}
#endif

	if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}





/*!
 * \brief
 * 	  Use the function ATE to send the ECHO mode on or off.
 * 	  1: Set the echo mode on
 * 	  0: Set the echo mode off
 * 	  By default the echo mode is on. With this function the mode
 * 	  is changed to off.
 * 	  In order to save the configuration setting AT&W must be called
 * 	  right after the execution of this command.
 *
 *\param sim80x,
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *	\arg SIM80_EMPTY
 *	\arg SIM80_TIMEOUT
 */

handle_status_en Sim80x_set_echo_mode (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_Gen_set_echo_mode,'\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
	return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;
	return SIM80_OK;
}



/*!
 *\brief
 * 	This function is used to send the AT+CMEE command toward the Sim80x Module
 *
 *\param sim80x
 *\param param, the parameter the user wants to send with the command.
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *	\arg SIM80_EMPTY
 *	\arg SIM80_TIMEOUT
 */
handle_status_en Sim80x_error_report (sim80x_t* sim80x, char* param, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_Gen_error_report, param);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}


/*!
 *\brief
 * 	Switch on or off detecting the SIM card, AT+CSDT=1||0
 * 	0: Switch off detecting the SIM card
 * 	1: Switch on detecting the SIM card
 *
 *\param sim80x
 *\param param, the parameter the user wants to send with the command.
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *	\arg SIM80_EMPTY
 *	\arg SIM80_TIMEOUT
 */
handle_status_en Sim80x_SIM_detect (sim80x_t* sim80x, char* param, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_Gen_detect_switch, param);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}


/*!
 * \brief
 * 	  AT&W0 command. The user can use it to store the current
 * 	  setting in the user profile 0. The list of all the settings
 * 	  from commands that can be stored are included in the datasheet
 * 	  in pages 47,48,49 SIMCom_Series_AT_Command_Manual_v1.09.pdf
 *
 *
 *\param sim80x,
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *	\arg SIM80_EMPTY
 *	\arg SIM80_TIMEOUT
 */

handle_status_en Sim80x_store_profile (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_Gen_store_profile,'\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;
	return SIM80_OK;
}



/*
 * =============== Public API declarations ================
*/










/*
 * =================== Public API declarations =====================
 */

/*!
 *\brief
 * 	The command AT is sent as the first command towards the SIM80x module.
 * 	Otherwise the module will not be initialized.
 *\param sim80x
 *\param mode, 0 if the user has already disabled the ECHO mode,
 *			   1 otherwise
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *	\arg SIM80_EMPTY
 *	\arg SIM80_TIMEOUT
 *
 */
handle_status_en Sim80x_set_AT (sim80x_t* sim80x, int mode, float input_timeout) {
	handle_status_en hstatus;
	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_Init_AT,'\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;
	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);

	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (mode); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}

/* Set and Get functions for the PIN insertion, a new pin insertion and the baud rate*/
void Sim80x_set_PIN(sim80x_t *sim80x, char *pin) {
	sim80x->pin = pin;
}

char* Sim80x_get_PIN(sim80x_t *sim80x) {
	return sim80x->pin;
}

void Sim80x_set_new_PIN(sim80x_t *sim80x, char *new_pin) {
	sim80x->new_pin = new_pin;
}

char* Sim80x_get_new_PIN(sim80x_t *sim80x) {
	return sim80x->new_pin;
}

void Sim80x_set_baud_rate(sim80x_t *sim80x, char *br) {
	sim80x->br = br;
}

char* Sim80x_get_baud_rate(sim80x_t *sim80x) {
	return sim80x->br;
}

void Sim80x_set_location(sim80x_t *sim80x, float longitude, float latitude) {
	sim80x->extracted_features->location.longitude = longitude;
	sim80x->extracted_features->location.latitude  = latitude;
}

void Sim80x_get_location(sim80x_t *sim80x, float *longitude, float *latitude) {
	*longitude = sim80x->extracted_features->location.longitude;
	*latitude  = sim80x->extracted_features->location.latitude;
}


/* Set the Phone number of the emergency support" */
void Sim80x_set_phone_number(sim80x_t *sim80x, char *phone_number) {
	int i=0;

	sim80x->phone_number[i] = '\"';
	for (i=1; i<strlen(phone_number)+1; ++i) {
		sim80x->phone_number[i] = phone_number[i-1];
	}
	sim80x->phone_number[i++] = '\"';
	sim80x->phone_number[i] = '\0';
}


/* Get the phone number */
char *Sim80x_get_phone_number(sim80x_t *sim80x) {
	return sim80x->phone_number;
}


/*\brief
 * 	This function appends a character at the end of a string
 * 	For example, an AT command like AT+CPIN?, where the '?' character must be
 * 	appended
 *
 *\param sim80x
 *\param new_pin, a string containing the new pin or a PUK
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *	\arg READY
 *	\arg SIM80_EMPTY
 *	\arg SIM80_TIMEOUT
 */

handle_status_en Sim80x_insert_PIN (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;

	/* If the length of the new pin is 4 characters long  a new pin will
	 * be inserted. Otherwise the pin will be inserted to the module
	 */

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_Init_enter_PIN, sim80x->pin);

	if (hstatus != SIM80_OK)
		return hstatus;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);

	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);

	if (hstatus == SIM80_SIM_ERROR || hstatus == SIM80_OTHER)
	{
		unsigned int hash_key = _Sim80x_extract_err_length ();
		sim80x->err_msg = _hash_search (_hash_table, hash_key);
		return SIM80_SIM_ERROR;
	}

	return SIM80_OK;
}


/*\brief
 * 	This function is used to get the PIN number from the SIM card
 *
 *\param sim80x,
 *\param pin, a pointer to uint32_t type
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 */
handle_status_en Sim80x_request_PIN (sim80x_t* sim80x, hash_t *_hash_table, SIM_codes_en *sstatus, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_Init_enter_PIN, '?');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus == SIM80_SIM_ERROR)
	{
		unsigned int hash_key = _Sim80x_extract_err_length ();
		sim80x->err_msg = _hash_search (_hash_table, hash_key);
		return SIM80_SIM_ERROR;
	}
	else if (hstatus == SIM80_OTHER)
		*sstatus = _Sim80x_extract_SIM_code (sim80x);


	//sprintf(sim80x->pin, "%u", _Sim80x_extract_length(sim80x));
	/* Probably bug. The response is either ready or something */	/*FIX: swap SIM80_OK and SIM80_SIM_ERROR in the enum Handle...*/
	return SIM80_OK;
}


handle_status_en Sim80x_insert_new_PIN (sim80x_t* sim80x, hash_t *_hash_table, float input_timeout) {
	handle_status_en hstatus;

	/* If the length of the new pin is 4 characters long  a new pin will
	 * be inserted. Otherwise the pin will be inserted to the module
	 */
	char* temp_pin = sim80x->pin;
	sprintf(temp_pin + strlen(temp_pin), "%c", ',');
	sprintf(temp_pin + strlen(temp_pin), sim80x->new_pin);
	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_Init_enter_PIN, temp_pin);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);

	if (hstatus == SIM80_SIM_ERROR || hstatus == SIM80_OTHER)
	{
		unsigned int hash_key = _Sim80x_extract_err_length ();
		sim80x->err_msg = _hash_search (_hash_table, hash_key);
		return SIM80_SIM_ERROR;
	}

	return SIM80_OK;
}
/*!
 * brief
 * 	This function sets the baud rate for the Sim80x module
 *\param sim80x, pointer to the struct sim80x_t
 *\param mode, 0 if the user has already disabled the ECHO mode,
 *			   1 otherwise
 *\param input_timeout, containing the user defined timeout
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 */

handle_status_en Sim80x_insert_baud_rate (sim80x_t* sim80x, int mode, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_Init_br, sim80x->br);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (mode); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;
	return SIM80_OK;
}


/*\brief
 * 	This function is used to get the baud rate
 * 	Call AT+IPR?
 *
 *\param sim80x
 *\param br, a pointer to uint32_t that represent
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 */
handle_status_en Sim80x_request_baud_rate (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_Init_br, '?');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (1); /* Clean the data */

//	hstatus = _Sim80x_check_response (sim80x);
//	if (hstatus != SIM80_OK)
//		return SIM80_SIM_ERROR;

	//Get baud rate
	sprintf (sim80x->br, "%u", _Sim80x_extract_length (sim80x));
	return SIM80_OK;
}


/*!
 * brief
 * 	This function is used for selecting the TEXT mode in the SMS
 * 	\param sim80x
 * 	\param format, it should be 1 or 0. 1 for text mode, 0 for PDU mode
 *
 * 	\return
 * 		\arg SIM80_OK
 * 		\arg SIM80_SIM_ERROR
 * 		\arg SIM80_TIMEOUT
 */

static handle_status_en _Sim80x_set_format_SMS (sim80x_t* sim80x, char* format, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_SMS_Msg_format, format);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;
	return SIM80_OK;
}


/*!
 * brief
 * 	This function is used for sending a SMS to the requested phone number.
 * 	\param sim80x
 * 	\param phone_number, it should be in the format: "+306978654321"
 * 	\param SMS, the Message to send. It is important to note that the string that contains the message should be
 * 		   by 1 character larger than expected due to the need of sending a <CTRL-Z> character as the last character
 * 	\param input_timeout, containing the user defined timeout
 *
 * 	\return
 * 		\arg SIM80_OK
 * 		\arg SIM80_SIM_ERROR
 */

handle_status_en Sim80x_send_SMS (sim80x_t* sim80x, char* phone_number, char* SMS, float input_timeout) {
	handle_status_en hstatus;

	/* Select TEXT format for the SMS */
	if ((hstatus = _Sim80x_set_format_SMS (sim80x, "1", input_timeout)) != SIM80_OK)
		return hstatus;

	if ((hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_SMS_Send, phone_number)) != SIM80_OK)
		return SIM80_SIM_ERROR;

	if ((hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout)) != SIM80_OK)
		return hstatus;

	if ((hstatus = _Sim80x_check_response (sim80x)) != SIM80_PROMPT)
		return SIM80_SIM_ERROR;

	sprintf (SMS + strlen (SMS), "%c", 26);			/*< Place the <CTRL-Z> character at the end of the string */
	Sim80x_send(sim80x, SMS, strlen(SMS), DATA);	/*< Send the data towards the Sim80x Module */

	if ((hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout)) != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter(0);		/* Clear the data */

	if ((hstatus = _Sim80x_check_response (sim80x)) != SIM80_OK)
		return SIM80_SIM_ERROR;

	if (sim80x->io.CheckReceive())
		sim80x->io.ReceiveFlush();
	return SIM80_OK;
}


/*!
 * brief
 * 	This function sets the functionality for the Sim80x module
 * 	\param sim80x, pointer to the struct sim80x_t
 * 	\param c, the string containing the 1 or 0. (1: Full functionality,
 * 												 0: Minimum Functionality
 *      										 4: Disable phone both transmit and receive RF circuits
 * 	\param input_timeout, containing the user defined timeout
 * 	\return
 * 		\arg SIM80_OK
 * 		\arg SIM80_SIM_ERROR
 */

handle_status_en Sim80x_set_functionality (sim80x_t* sim80x, char* c, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_Init_set_funct,c);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;
	return SIM80_OK;
}


handle_status_en Sim80x_set_registration (sim80x_t* sim80x, char* c, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_GPRS_ntwrk_reg, c);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

//	hstatus = _Sim80x_check_response (sim80x);
//	if (hstatus != SIM80_OK)
//		return SIM80_SIM_ERROR;
//	*rstatus = _Sim80x_extract_registration(sim80x);
	return SIM80_OK;
}

//AT+CREG?
/*!
 * brief
 * 	This function get the registration info for the Sim80x module
 * 	\param sim80x, pointer to the struct sim80x_t
 * 	\param input_timeout, containing the user defined timeout
 * 	\param rstatus, a variable of registration_en type called by reference
 * 		   			in order to acknowledge from main the status of the
 * 		   			registration
 * 	\return
 * 		\arg SIM80_OK
 * 		\arg SIM80_SIM_ERROR
 */
handle_status_en Sim80x_get_registration (sim80x_t* sim80x, float input_timeout, registration_en *rstatus) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_GPRS_ntwrk_reg,'?');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus == SIM80_SIM_ERROR)
		return SIM80_SIM_ERROR;

	*rstatus = _Sim80x_extract_registration(sim80x);	// the value that the pointer point to interests us.
	return SIM80_OK;
}




/*!
* \brief
*	Send an AT Command to the module SIM80x and wait for the response.
*	At the end of the Command, a \r character is necessary to be sent.
*
*\param command, the AT Command to send to the SIM80x module.
*\param c, if character '?' is sent, an AT Command with a ? at the end will be sent
*\return
*	\arg SIM80_OK
*	\arg SIM80_SIM_ERROR
*/

handle_status_en Sim80x_send_AT_Command (sim80x_t *sim80x, char* command, char c) {

	memset(current_command, '\0', strlen(current_command));
	_Sim80x_set_AT_command (command);

	char* finalized_command = _Sim80x_append(current_command, c);
	size_t size = strlen(finalized_command);
	return Sim80x_send(sim80x, finalized_command, size, AT_COMMAND);
	// The return status of the AT command execution to the SIM80x module.
}





/*!
* \brief
*	Send data as ASCII characters to the tx queue in order
*	to send them through UART to the SIM80x module.
*\param dataSend, a string that contains the data to be sent.
*\param send_mode, AT_COMMAND for AT command mode, DATA for data sent mode
*\return
*	\arg SIM80_OK
*	\arg SIM80_SIM_ERROR
*	\arg FULL_BUFFER
*/

handle_status_en Sim80x_send (sim80x_t *sim80x, void* dataSend, size_t size, send_mode_en send_mode) {

	char *cur_char = (char*)dataSend;

	for (int i=0; i < size; ++i) {
		sim80x->io.Putchar(*cur_char);
		if (send_mode == AT_COMMAND && *cur_char == '\r')		//Termination character of the command
			return SIM80_OK;

		cur_char++;
	}
	if (send_mode == DATA) {
//		sim80x->io.ReceiveFlush();
		return  SIM80_OK;
	}
	return SIM80_SIM_ERROR;
}








/*!
* \brief
*	Creates a setter function for the probable AT command parameters.
*	This appends the desired parameters to the respective AT command and executes it.
*\param command, the command to send
*\param parameters, a string containing the parameters we want to send with the command.
*		For example, for the command AT+IPR="115200" the parameters variable is "115200"
*
*\return
*	\arg SIM80_OK
*	\arg SIM80_SIM_ERROR
*/

handle_status_en Sim80x_send_AT_Command_with_parameters (sim80x_t *sim80x, char* command, char* parameters) {
													// For TCP (2nd argument) -> 0, UDP -> 1.
													// The parameters will be given as one continuous string, i.e.
													// "param1,param2,param3" -> "1,TCP,8080".
													// To obtain each argument,
													// generally, we could also use the strtok() function.
	int i=0, j=0;
	int command_length = strlen(command);
	int parameters_length = strlen(parameters);
	i = command_length;

	memset(current_command, '\0', strlen(current_command));
	current_command[i] = '=';
	++i;
	for (; i < (command_length + parameters_length + 1); ++i) {
		current_command[i] = parameters[j];
		j++;
	}
//	sprintf(current_command, parameters);	//Another implementation
	handle_status_en sendATCommand_ret_st = Sim80x_send_AT_Command (sim80x, command, 0);
	return sendATCommand_ret_st;
}


/* ==================================================================
 * ================			PRIVATE GPRS FUNCTIONS		=============
 *===================================================================
 */


/*!
 *\brief
 * 	This function extracts the data size that is included in the header data that the server sends at the beginning of
 * 	its transmission.
 *\param sim80x
 *\return data_size
 *
 */

int _GPRS_extract_data_size(sim80x_t *sim80x) {

	char copy_ip_head[13];
	char *copy_ip_head_p, *start, *end;
	int data_size;

	char delimit[6] = "+IPD,";
	for (int i=0; i<strlen(sim80x->ip_head); ++i) {
		copy_ip_head[i] = sim80x->ip_head[i];
	}

	copy_ip_head_p = &copy_ip_head[0];

	start = strstr(copy_ip_head_p, delimit);	// ...+IPD,65535:... -> start points to ...<+>IPD,65535:...
	start += 5;	// start now points to the first digit of the data size -> ...+IPD,<6>5535:...
	end = strstr(start, ":");	//i.e.: \r\n+IPD,243:	->	start ---> 2 (position = 7),	end ---> : (position = 10)

	end[0] = '\0';
	sscanf (start, "%d", &data_size);
	return data_size;
}


static handle_status_en _GPRS_DNS_IP_tx_rx_query (sim80x_t* sim80x, char *param, float input_timeout)
{
	handle_status_en hstatus;

	if (param != NULL)	{
		hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_DNS_IP_query, param);
		if (hstatus != SIM80_OK)
			return SIM80_SIM_ERROR;
	}
	else if (param == NULL)	{
		hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_DNS_IP_query, sim80x->GPRS->sim_domain);
		if (hstatus != SIM80_OK)
			return SIM80_SIM_ERROR;
	}

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus == SIM80_SIM_ERROR)
		return SIM80_SIM_ERROR;
	else if (hstatus == SIM80_TIMEOUT)
		return SIM80_TIMEOUT;

	_Sim80x_recv_filter (0); /* Clean the data */
	return SIM80_OK;
}


/* =================================================================
 * ===============		 PUBLIC GPRS FUNCTIONS		 ===============
 * =================================================================
 */

handle_status_en GPRS_Init (sim80x_t* sim80x, attach_detach_en *astatus)
{
	clock_t now, mark, gprs_now;
	handle_status_en hstatus;

	strcpy(sim80x->GPRS->Gen_dns_com_info, sim80x->GPRS->protocol);
	sprintf(sim80x->GPRS->Gen_dns_com_info + strlen(sim80x->GPRS->Gen_dns_com_info), "%c", ',');
	sprintf(sim80x->GPRS->Gen_dns_com_info + strlen(sim80x->GPRS->Gen_dns_com_info), sim80x->GPRS->sim_domain);
	sprintf(sim80x->GPRS->Gen_dns_com_info + strlen(sim80x->GPRS->Gen_dns_com_info), "%c", ',');
	sprintf(sim80x->GPRS->Gen_dns_com_info + strlen(sim80x->GPRS->Gen_dns_com_info), sim80x->GPRS->port);


/*  ======================================================================
 * 	===========  	AT+CGATT=1, attach to GPRS Network			==========
 *	======================================================================
 */
	if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

#ifdef DEBUG

		while ((hstatus = GPRS_attach_detach(sim80x, "1", CGATT_TIMEOUT)) != SIM80_OK) {
			now = clock();
			while (_CLOCK_DIFF (clock (), now) <= 100)
				;
			++k;
		}
#else
		gprs_now = clock();
		while (_CLOCK_DIFF(clock(), gprs_now) <= CGATT_TIMEOUT*1000) {				/*< Wait for 20 seconds max to connect to GPRS    */
			hstatus = SIM80_SIM_FUN_ERROR;
			if ((hstatus = GPRS_attach_detach(sim80x, "1", CGATT_TIMEOUT)) == SIM80_OK)	/*< Attempt to attach to GPRS (each try max 10sec */
				break;
			else {
				now = clock();
				while (_CLOCK_DIFF (clock (), now) <= 100)
					;
			}
		}
#endif
	if (hstatus != SIM80_OK)
		return hstatus;

	if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

	gprs_now = clock();
	while (_CLOCK_DIFF(clock(), gprs_now) <= GENERAL_TIMEOUT* get_freq()) {				/*< Wait for 20 seconds max to connect to GPRS    */
		hstatus = SIM80_SIM_FUN_ERROR;
		if ((hstatus = GPRS_insert_APN(sim80x, GENERAL_TIMEOUT)) == SIM80_OK)	/*< Attempt to attach to GPRS (each try max 10sec */
			break;
		else {
			now = clock();
			while (_CLOCK_DIFF (clock (), now) <= 100)
				;
		}
	}
	if (hstatus != SIM80_OK)
		return hstatus;

	/* Execute the AT+CIICR Command to bring up the GPRS connection */
	if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

	while (_CLOCK_DIFF(clock(), gprs_now) <= GENERAL_TIMEOUT* get_freq()) {
		hstatus = SIM80_SIM_FUN_ERROR;
		if ((hstatus = GPRS_start(sim80x, GENERAL_TIMEOUT)) == SIM80_OK)
			break;
	}

	if (hstatus != SIM80_OK)
		return hstatus;

    if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

    mark = clock();
	do {
		now = clock();
       if (_CLOCK_DIFF(now, mark) >= INPUT_TIMEOUT* get_freq())
          break;
	   hstatus = GPRS_get_local_ip(sim80x, GENERAL_TIMEOUT);
	} while (!((sim80x->extracted_features->localIP[0] >= 48) &&   \
 		       (sim80x->extracted_features->localIP[0] <= 57)));

	if (hstatus != SIM80_OK)
		return SIM80_SIM_FUN_ERROR;


    if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();




	return SIM80_OK;
}





/*!
 * \brief
 * 	  The below functions are a set of functions used by the API
 * 	  in order to set the appropriate configurations for the GPRS
 * 	  Communication.
 */


 /* Set the APN of the ISP (for example "internet.vodafone.gr" */
void GPRS_set_APN(sim80x_t *sim80x, char *apn_in) {
	int i=0;

	sim80x->GPRS->sim_apn[i] = '\"';
	for (i=1; i<strlen(apn_in)+1; ++i) {
		sim80x->GPRS->sim_apn[i] = apn_in[i-1];
	}
	sim80x->GPRS->sim_apn[i++] = '\"';
	sim80x->GPRS->sim_apn[i] = '\0';
}


/* Get the APN of the ISP */
char *GPRS_get_APN(sim80x_t *sim80x) {
	return sim80x->GPRS->sim_apn;
}



/* Set domain */
void GPRS_set_Domain(sim80x_t *sim80x, char *domain_in) {
	int i=0;
	sim80x->GPRS->sim_domain[i] = '\"';
	for (i=1; i<strlen(domain_in)+1; ++i)	{
		sim80x->GPRS->sim_domain[i] = domain_in[i-1];
	}
	sim80x->GPRS->sim_domain[i++] = '\"';
	sim80x->GPRS->sim_domain[i] = '\0';
}


/* Get DNS*/
char *GPRS_get_Domain(sim80x_t *sim80x) {
	return sim80x->GPRS->sim_domain;
}


/* Set the protocol use (\"TCP\"  or  \"UDP\") */
void GPRS_set_protocol(sim80x_t *sim80x, char *protocol) {
	int i=0;

	sim80x->GPRS->protocol[i] = '\"';
	for (i=1; i<strlen(protocol)+1; ++i) {
		sim80x->GPRS->protocol[i] = protocol[i-1];
	}
	sim80x->GPRS->protocol[i++] = '\"';
	sim80x->GPRS->protocol[i] = '\0';
}


/* Get the protocol that is currently used */
char *GPRS_get_protocol(sim80x_t *sim80x) {
	return sim80x->GPRS->protocol;
}



void GPRS_set_port(sim80x_t *sim80x, char *port) {
	int i=0;

	sim80x->GPRS->port[i] = '\"';
	for (i=1; i<strlen(port)+1; ++i) {
		sim80x->GPRS->port[i] = port[i-1];
	}
	sim80x->GPRS->port[i++] = '\"';
	sim80x->GPRS->port[i] = '\0';
}



char *GPRS_get_port(sim80x_t *sim80x) {
	return sim80x->GPRS->port;
}



//AT+CSGS?
/*!
 * brief
 * 	This function gets the registration status for the Sim80x module
 * 	using the netlight indication LED. This function was supposed to provide better
 * 	control results than the CREG AT Command. However, this assertion was proven wrong
 * 	since the Sim80x module does not respond correctly to any of the above 2 AT Commands.
 *
 * 	\param sim80x, pointer to the struct sim80x_t
 * 	\param input_timeout, containing the user defined timeout
 * 	\param
 * 	\return
 * 		\arg SIM80_OK
 * 		\arg SIM80_SIM_ERROR
 */
handle_status_en GPRS_get_netlight_status (sim80x_t* sim80x, registration_en *rstatus, float input_timeout)
{
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_GPRS_netlight_status,'?');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	else {
		if (strstr(receivedDataBuffer_fromSim80x, "0"))
			*rstatus = DISABLED_NETWORK;
		else if (strstr(receivedDataBuffer_fromSim80x, "1"))
			*rstatus = ENABLED_NETWORK;
		else if (strstr(receivedDataBuffer_fromSim80x, "2"))
			*rstatus = REGISTERED;
	}
	return SIM80_OK;
}



//AT+CIICR, start up GPRS connection
/*!
 * brief
 * 	This function executes the AT+CIICR command in order to initiate a
 * 	communication process over GPRS
 * 	\param sim80x, pointer to the struct sim80x_t
 * 	\param input_timeout, containing the user defined timeout
 * 	\return
 * 		\arg SIM80_OK
 * 		\arg SIM80_SIM_ERROR
 */

handle_status_en GPRS_start (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_GPRS_start_con, '\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK  &&  hstatus!= SIM80_SMS_Ready)
		return SIM80_SIM_ERROR;
	return SIM80_OK;
}

//AT+CGATT (attach or detach from the network)
/*!
 * brief
 * 	This function is used to attach or detach to and from the GPRS network
 * 	\param sim80x, pointer to the struct sim80x_t
 * 	\param c, 1: Attach, 0: Detach
 * 	\param input_timeout, containing the user defined timeout
 *
 * 	\return
 * 		\arg SIM80_OK
 * 		\arg SIM80_SIM_ERROR
 */
handle_status_en GPRS_attach_detach (sim80x_t* sim80x, char* c, float input_timeout) {
	handle_status_en hstatus;

	for (int i=0; i<strlen(receivedDataBuffer_fromSim80x); ++i) {
		receivedDataBuffer_fromSim80x[i] = '\0';
	}
	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_GPRS_attach_or_detach, c);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;


	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;
	return SIM80_OK;
}

//AT+CGATT?
/*!
 * \brief
 *	 Question towards the network to acknowledge
 *	 whether or not the SIM80x module is attached
 *	 to the network.
 *\param sim80x
 *\param  input_timeout
 *\return
 *	  \arg SIM80_OK
 *	  \arg SIM80_SIM_ERROR
 */
handle_status_en GPRS_attach_detach_query (sim80x_t* sim80x, hash_t *_hash_table, attach_detach_en *astatus, float input_timeout)
{
	handle_status_en hstatus;
	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_GPRS_attach_or_detach, '?');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	_Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if ((hstatus !=SIM80_SIM_ERROR)  &&  (strstr(receivedDataBuffer_fromSim80x, "0")) != NULL)
	{
		*astatus = DETACHED;
		return SIM80_OK;
	}
	else if ((hstatus !=SIM80_SIM_ERROR)  &&  (strstr(receivedDataBuffer_fromSim80x, "1")) != NULL)
	{
		*astatus = ATTACHED;
		return SIM80_OK;
	}
	else
	{
		if (hstatus == SIM80_SIM_ERROR)
		{
			unsigned int hash_key = _Sim80x_extract_err_length();
			sim80x->err_msg = _hash_search(_hash_table, hash_key);
			return SIM80_SIM_ERROR;
		}
	}

	return SIM80_OK;
}



// ==========AT+CIPSTART="\"mode\",\"domain name\",\"port\""============

/*!
 *\brief
 * 	One of the most important function of this library.
 * 	Using the specific command the initialization of the DNS communications over TCP
 * 	is achieved.
 * 	According to datasheet, were an error for this command occurs,
 *  it is suggest to use the AT+CIPCLOSE command and restart the
 *  connection. If the problem remains, call AT+CIPSHUT. If still a problem exists,
 *  reset the module. User must wait for the responses: SIM80_OK, CONNECT SIM80_OK in order to be sure
 *  that the connections have been established successfully.
 *
 *\param sim80x
 *\param param, the parameter the user wants to send with the command.
 *		 The string to be sent must have the following format: "\"mode\",\"domain name\",\"port\""
 *		 Mode is either TCP or UDP.
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 */
handle_status_en GPRS_start_TCP_UDP_DNS_con (sim80x_t* sim80x, float input_timeout)
{
	/* In order to avoid as many errors as possible, before the initialization of the communication
	 * it is considered wise to flush the buffer.Thus, since unsuccessful trials of the command might
	 * happen the best thing we can do is flush the buffer.
	 */
	if (sim80x->io.CheckReceive() != 0)
		sim80x->io.ReceiveFlush();

	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_TCP_Init, sim80x->GPRS->Gen_dns_com_info);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

//	_Sim80x_recv_filter (0); /* Clean the data */
	hstatus = _Sim80x_check_response (sim80x);
	if ((hstatus ) == SIM80_SIM_ERROR)
	{
		unsigned int hash_key = _Sim80x_extract_err_length();
		sim80x->err_msg = _hash_search(_hash_table, hash_key);
		if (sim80x->err_msg == NULL)
			sim80x->err_msg = "ERROR";
		return hstatus;
	}
	else if (hstatus == SIM80_CONNECT_OK  ||  hstatus == SIM80_ALREADY_CONNECTED)
		return hstatus;

	return SIM80_SIM_ERROR;
}



//AT+CIPMUX, start up single or multi connection (0: Single, 1: Multiple)

/*!
 *\brief
 * 	Choose between single and multiple connection (0: Single, 1: Multiple)
 *\param sim80x
 *\param param, "1" or "0"
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 */

handle_status_en GPRS_single_multiple (sim80x_t* sim80x, char* param, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_TCP_IP_start_con, param);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}





//AT+CIPSEND? It is important to find out the maximum length that can be sent to
//the network.

/*!
 *\brief
 * 	Find the maximum length that can be sent to the network. It is determined
 * 	by the network.
 *\param sim80x
 *\param size, the length of data that is possible to send. Call by reference
 *		       in order to use it outside the function
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 */
handle_status_en GPRS_avail_data_length_to_send (sim80x_t* sim80x, size_t* size, float input_timeout)
{
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_Gen_send_data, '?');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	*size = _Sim80x_extract_length(sim80x);	//The max length that can be sent to the network

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}


//AT+CSQ Checks signal quality
/*!
 *\brief
 * 	Using this function, the quality of signal can be determined.
 * 	For example, if the quality of signal is low, the module does not
 * 	have to initialize an attempt to send/receive data.
 *\param sim80x
 *\param qstatus, a pointer to the struct QoS_en
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 */
handle_status_en GPRS_check_signal (sim80x_t* sim80x, QoS_en *qstatus, BER_en *bstatus, float input_timeout)
{
	handle_status_en hstatus;

    if (sim80x->io.CheckReceive() != 0)
		 sim80x->io.ReceiveFlush();

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_Gen_check_qos, '\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	*qstatus = _Sim80x_extract_QoS(sim80x);	//Returns a struct obj of QoS_en
//	*bstatus = _Sim80x_extract_BER(sim80x);


    if (sim80x->io.CheckReceive() != 0)
		 sim80x->io.ReceiveFlush();

	return SIM80_OK;
}


//AT+CIFSR
/*!
 *\brief
 * 	Get the local ip address
 *\param sim80x
 *\param input_timeout
 *\param ip, the local ip address
 *
 */
handle_status_en GPRS_get_local_ip (sim80x_t* sim80x, float input_timeout) {

	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_Gen_IP_get_local, '\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	_Sim80x_extract_IPandPORT(sim80x,0);	//Need only the IP address. The port is not returned from this function
										//The IP is stored in the struct extracted_features_t
	return SIM80_OK;
}


//AT+CIPCLOSE, Close TCP connection
//RESPONSE: SIM80_OK or ERROR
/*!
 *\brief
 * 	Using this function the TCP connection closes.
 *\param sim80x
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 */
handle_status_en GPRS_TCP_close (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;
	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_TCP_close, '\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_CLOSE_OK)
		return SIM80_SIM_ERROR;

	return hstatus;
}

//AT+CIPSHUT, Close GPRS PDP connection
//RESPONSE: SIM80_OK or ERROR
/*!
 *\brief
 * 	Using this function the GPRS connection closes.
 *\param sim80x
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 */
handle_status_en GPRS_PDP_close (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;
	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_GPRS_deact_PDP, '\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_SHUT_OK)
		return SIM80_SIM_ERROR;

	return hstatus;
}


//AT+CDNSGIP, Query the IP Address of Given Domain Name
/*!
 *\brief
 * Query the IP Address of a given domain name
 *\param sim80x
 *\param domain_name, a string that contains the domain name of the server the
 *		 			  module wants to communicate
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *	\arg DNS_ERROR
 */
handle_status_en GPRS_DNS_IP_query (sim80x_t* sim80x, char *param, float input_timeout) {
	handle_status_en hstatus;

	char temp_dns[254];		// The maximum Domain name is 253 characters.
	temp_dns[0] = '\"';
	sprintf (temp_dns + strlen(temp_dns), param);
	sprintf (temp_dns + strlen(temp_dns), "%c", '\"');

	hstatus = _GPRS_DNS_IP_tx_rx_query(sim80x, temp_dns, input_timeout); // edit this for secondary IP

	if(hstatus != SIM80_OK)
		return hstatus;

	if (receivedDataBuffer_fromSim80x[6] == '+') {
//			while (receivedDataBuffer_fromSim80x[6] != '+') {
//				now = clock();
//				while (_CLOCK_DIFF (clock (), now) <= diffussion)
//					;
//				test_GPRS_DNS_IP_query(&Sim80x, "\"www.facebook.com\"", 10);
//			}

		if (receivedDataBuffer_fromSim80x[16] == '1') {
			_Sim80x_extract_DNSandIP(sim80x, 2);
			hstatus = SIM80_OK;
			return hstatus;

		}
		else if (receivedDataBuffer_fromSim80x[16] == '0') {
			hstatus = SIM80_DNS_ERROR;
			return hstatus;
		}

	}
	else if (receivedDataBuffer_fromSim80x[0] == '+') {
//			while (receivedDataBuffer_fromSim80x[0] != '+') {
//				now = clock();
//				while (_CLOCK_DIFF (clock (), now) <= diffussion)
//					;
//				test_GPRS_DNS_IP_query(&Sim80x, "\"www.facebook.com\"", 10);
//			}
			//_Sim80x_extract_DNSandIP(sim80x, 2);
		if (receivedDataBuffer_fromSim80x[10] == '1') {
			_Sim80x_extract_DNSandIP(sim80x, 2);
			hstatus = SIM80_OK;
			return hstatus;
		}
		else if (receivedDataBuffer_fromSim80x[10] == '0') {
			hstatus = SIM80_DNS_ERROR;
			return hstatus;
		}
	}

	return SIM80_DNS_ERROR;
}



/*	AT+CIPACK	Query previous connection data transmitting state
 *	RESPONSE: +CIPACK: <>,<>,<>
 */

/*!
 *\brief
 * 	Query previous connection data transmitting state
 *\param sim80x
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *
 */

handle_status_en GPRS_query_previous_transm_status (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_TCP_prev_con_query, '\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	if ((hstatus = _Sim80x_extract_ack_status(sim80x)) != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}



//AT+CIPQSEND=a
//			 a: 0 = Normal (Default)
//			 	1 = Quick Send Mode
//RESPONSE: SIM80_OK

/*!
 *\brief
 * 	Definition of normal or transparent mode
 *\param sim80x
 *\param sel_mode, a string containing the "1" or "0"
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *
 */

handle_status_en GPRS_sel_transm_mode (sim80x_t* sim80x, char* sel_mode, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_TCP_IP_trans_mode, sel_mode);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;
	return SIM80_OK;

}


//AT+CIPHEAD

/*!
 *\brief
 * 	Add a header at the beginning of a package received
 *	0: not add ip header
 *	1: add ip header
 *	If 1, received packets will have the format: +IPD(data length): payload
 *\param sim80x
 *\param sel_mode
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *
 */
handle_status_en GPRS_ip_head (sim80x_t* sim80x, char* sel_mode, float input_timeout) {
	handle_status_en hstatus;
	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_TCP_IP_header, sel_mode);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;
	return SIM80_OK;
}



//AT+CSTT
//Start task and set APN, username and password

/*!
 *\brief
 * 	Start the task and set APN, username and password
 *\param sim80x
 *\param parameters, a string containing the parameters, separated with a comma
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *
 */
handle_status_en GPRS_insert_APN (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;
	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_TCP_APN_usr_pwd, sim80x->GPRS->sim_apn);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK  &&  hstatus != SIM80_CHINESE  && hstatus != SIM80_SMS_Ready)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}


//AT+CLPORT=<mode>,<port>, port from 1...65535
//Set local port
//Default value of the port is 0.
//This means that the port is dynamically allocated to a port

/*!
 *\brief
 * 	Set local port
 *\param sim80x
 *\param parameters, a string that contains the mode and the port in the format
 *					 "mode,port"
 *\param input_timeout
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *
 */

handle_status_en GPRS_set_local_port (sim80x_t* sim80x, char* parameters, float input_timeout) {
	handle_status_en hstatus;
	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_TCP_set_loc_port, parameters);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /*!< Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}


/*!
 * \brief
 * 	  This function returns the number of items in the rx buffer
 * 	  by calling the sim80x->io.CheckReceive() function
 */
//int GPRS_CheckReceive(sim80x_t *sim80x) {
//	return sim80x->io.CheckReceive();
//}

/*!
 *\brief
 *    This function extracts the data size that is included in the header data that the server sends at the beginning of
 *    its transmission. In case of not receiving anything for a specific time the function returns 0.
 *
 *    - It is very important to notice inside the "do...while()" the init sequence.
 *    - If the number of characters inside the rx_buffer is less than 10, then the
 *      function will either wait or return 0 if the timeout explodes.
 *    - The reason why the number of the characters we wait is set to 10 is
 *      because the Sim80x Module before every packet sends the sequence:
 *
 *      			"\r\n+IPD,<size_of_payload>:<payload>"
 *
 *     - With that being said we can observe that the number of characters before the
 *       payload is 9. Hence, we wait for another character to arrive in order to avoid
 *       any problems regarding with any false sent from the Sim80x Module
 *
 *\param sim80x
 *\return data_size
 */
int GPRS_extract_data_size(sim80x_t *sim80x) {

    int data_size=0;
    int i=0;
    char *copy_ip_head_p, *start, *end;
    uint8_t flag=0;
    char copy_ip_head[13];

	if (sim80x->io.CheckReceive() >= 10) {
		flag = 1;
	    while (!((sim80x->ip_head[i++] = (char)sim80x->io.Getchar()) == ':'))
	   		;
	}

    if (!flag)		//If the ':' has not showed up, it means that data have not been sent to the GSM Module
    	return 0;
    else {
		char delimit[6] = "+IPD,";
		for (int j=0; j<strlen(sim80x->ip_head); ++j) {
			copy_ip_head[j] = sim80x->ip_head[j];
		}

		copy_ip_head_p = &copy_ip_head[0];

		start = strstr(copy_ip_head_p, delimit);  // ...+IPD,65535:... -> start points to ...<+>IPD,65535:...
		start += 5; // start now points to the first digit of the data size -> ...+IPD,<6>5535:...
		end = strstr(start, ":");  //i.e.: \r\n+IPD,243:   -> start ---> 2 (position = 7),  end ---> : (position = 10)

		end[0] = '\0';
		sscanf (start, "%d", &data_size);
    }

	return data_size;
}

/*!
 * \brief
 * 	  This function is responsible for the receive of data
 * 	  from the server. If the user wants to use the NON-BLOCKING
 * 	  mode and the timeout is 0, the CheckReceive() function checks
 * 	  the buffer. If empty return SIM80_EMPTY. Else, is the mode is BLOCKING
 * 	  the application remains in the function until the timeout is
 * 	  finished.
 *
 *\param getData, an array in which the received data from the server
 *		 will be stored.
 *\param size, the size i want to retrieve from the rx_buffer. If the size is bigger
 *\param
 *\param timeout, a float containing the duration of the timeout in seconds.
 */
handle_status_en GPRS_getData (sim80x_t *sim80x, void* getData, size_t size, float timeout) {

	uint32_t j=0;		//Might be used if sprintf is wrong
	clock_t now, total;
	//char cur_char[12] ;//(char*)getData;

	now = clock();
	total = timeout*get_freq();		//!< Timeout is expressed in seconds
	memset(sim80x->ip_head, '\0', strlen(sim80x->ip_head));
	while ((_CLOCK_DIFF (clock (), now) <= total)) {
		if (!sim80x->io.CheckReceive() && timeout == 0)
			return SIM80_EMPTY;
		else if(!sim80x->io.CheckReceive())
			continue;
//		if (sim80x->io.CheckReceive()  &&  i == 0)	{	//!< While the rx buffer is empty perform iteration
//			sim80x->ip_head[i++] = (char)sim80x->io.Getchar();
//			flag = 1;
//			continue;
//		}
//		else if (sim80x->io.CheckReceive()) {
//			if ((sim80x->ip_head[i++] = (char)sim80x->io.Getchar()) == ':')
//				flag = 0;
//		}
		else {
//			if (j == 0)
//				sim80x->recvdData_size = GPRS_extract_data_size(sim80x);
			if (size > j)
				((char*)getData)[j++] = (char)sim80x->io.Getchar();
			else
				break;
		}
/* Use as an alternative function to store the received characters from the rx buffer
*/
	}

	/* If the ip head that is received does not come as whole
	 * Normally, since first the GPRS_extract_data_size function is called to acquire
	 * the current payload size inside the rx_buffer there is no point in checking the j.
	 * However, since faults occur, this check helps to avoid any errros.
	 */
	if(j == 0)
		return SIM80_EMPTY;
	else
		((char*)getData)[j] = '\0';

	return SIM80_OK;
}


//AT+CIPSEND="size" , send data over TCP or UDP connection with SIM80x module
/*!
 *\brief
 *	This function uses the AT+CIPSEND command in order to send data over TCP or UDP
 *\param sim80x,
 *\param input_timeout,
 *\param data, the data to sent
 *\param size, the size of the data
 *
 *\return
 *	\arg SIM80_OK
 *	\arg SIM80_SIM_ERROR
 *	\arg SIM80_PROMPT
 */
handle_status_en GPRS_send_data (sim80x_t* sim80x, void* data, size_t size, float input_timeout)
{
	clock_t mark;
	uint8_t i=0;
	handle_status_en hstatus;

	char temp[25];

	sprintf(temp, sim80x->AT->_Gen_send_data);
	sprintf(temp + strlen(temp), "=");
	sprintf(temp + strlen(temp), "%u", size);
	hstatus = Sim80x_send_AT_Command (sim80x, temp, '\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_PROMPT) {
		unsigned int hash_key = _Sim80x_extract_err_length();
		sim80x->err_msg = _hash_search(_hash_table, hash_key);
		if (sim80x->err_msg == NULL)
			sim80x->err_msg = "ERROR";
		return SIM80_SIM_ERROR;
	}
	//If the prompt character '>' is the response of the SIM80x module
	//it means that it is ready to receive data
	Sim80x_send(sim80x, data, size, DATA);

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	hstatus = _Sim80x_check_response (sim80x);		/* Check the response. Expect \r\nSEND SIM80_OK\r\n */
	if (hstatus == SIM80_SEND_OK)
		return SIM80_OK;
	else {
		mark = clock();
		memset(check_receivedDataBuffer_fromSim80x, '\0', GSM_UART_BUFFER_SIZE);

		while (sim80x->io.CheckReceive()  &&  _CLOCK_DIFF(clock(), mark) < input_timeout) {
			check_receivedDataBuffer_fromSim80x[i++] = (char)sim80x->io.Getchar();
		}

		hstatus = _Sim80x_check_response (sim80x);

		if (hstatus != SIM80_SEND_OK)
			return SIM80_SIM_ERROR;
	}
	return SIM80_OK;
}




//=================     AT+CLTS=1       ================
/*!
 * \brief
 * 	  The user can use this function to set the Module in such a way that synchronizes its
 * 	  RTC clock with the current clock it receives from an ntp server. In our case we use
 * 	  time.google.com ntp server but there are many available.
 *
 */
static handle_status_en Gen_set_lcl_timestamp (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters(sim80x, sim80x->AT->_Gen_set_time, "1");
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	hstatus = Sim80x_store_profile(sim80x, input_timeout);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}

/*!
 * \brief
 * 	  By calling this function we request the current UTC time from an ntp server.
 */
static handle_status_en Gen_set_ntp_server (sim80x_t* sim80x, char* ntp_server, float input_timeout) {
	handle_status_en hstatus;

	char ntp_temp[51];
	sprintf (ntp_temp, sim80x->AT->_Gen_ntp);
	sprintf (ntp_temp + strlen(ntp_temp), "%s", "=\"");
	sprintf (ntp_temp + strlen(ntp_temp), ntp_server);
	sprintf (ntp_temp + strlen(ntp_temp), "%c", '\"');

	hstatus = Sim80x_send_AT_Command (sim80x, ntp_temp, '\r');//sim80x->AT->_Gen_set_time, "1");
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
	return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}

/*!
 * \brief
 *   The execution of this command is essential for the synchronization of the RTC with the
 *   ntp server's time
 */
static handle_status_en Gen_ntp_sync (sim80x_t* sim80x, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command(sim80x, sim80x->AT->_Gen_ntp, '\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}



/*!
 *\brief
 * 	 This function is used to set the bearer profile for the Sim80x Module
 */
handle_status_en Gen_BearerProfile (sim80x_t* sim80x, char* param, float input_timeout) {
	handle_status_en hstatus;

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_Gen_bearer_profile, param);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES1, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}


/*!
 * \brief
 * 	  This function gets either the current location of the device either the current data
 * 	  or both. The parameters are the same as with the most functions of this library
 * 	  For Longitude and Latitude the param should be 1. For Time only it should be 2
 * 	  The param should be "1,1" for Location.
 * 	  For more details refer to AT_Command Manual v1.09.pdf page 366
 */
handle_status_en Gen_Loc_and_Date (sim80x_t* sim80x, char* param, float input_timeout) {
	handle_status_en hstatus;

	clock_t now;

	// Initially the AT+SAPBR="3,1,\"Contype\",\"GPRS\"" is executed
//	hstatus = Sim80x_Sapbr(sim80x, "3,1,\"Contype\",\"GPRS\"", input_timeout);
	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()){
		if ((hstatus = Sim80x_Sapbr(sim80x, "3,1,\"Contype\",\"GPRS\"", input_timeout)) == SIM80_OK)
			break;
	}
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;


/* The user has inserted the apn name in the format: "internet.vodafone.gr". The Sim800 Module
 * requires the format: "\"internet.vodafone.g\"". That's why the sprintf's are used. To append
 * the \" characters to the string
 */
	char param2[60] = "3,1,\"APN\",";
	sprintf (param2 + strlen(param2), sim80x->GPRS->sim_apn);

	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Sim80x_Sapbr(sim80x, param2, input_timeout)) == SIM80_OK)
			break;
	}
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if (Sim80x_flag)	break;
		if ((hstatus = Sim80x_Sapbr(sim80x, "1,1", input_timeout)) == SIM80_OK) {
			Sim80x_flag ++;
			break;
		}
	}
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	/* Flush the buffer in order to avoid any mistakes from the Sim80x Module  */
	if (sim80x->io.CheckReceive())
		sim80x->io.ReceiveFlush();

	hstatus = Sim80x_send_AT_Command_with_parameters (sim80x, sim80x->AT->_Gen_Loc_Date, param);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
		return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	hstatus = _Sim80x_check_response (sim80x);
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	/* Extract the location of the GSM Module using the Location that has been received from the ISP */
	if ((hstatus = _Sim80x_extract_Location(sim80x)) != SIM80_OK)
		return SIM80_SIM_ERROR;

	return SIM80_OK;
}







/*!
 * \brief
 *	  This function is used to initialize the procedure of acquiring the current date and
 *	  time from the module
 *	  Inside this function all the necessary commands for acquiring the timestamp are executed
 *	  The reason why the specific course is followed and the commands are executed in that order
 *	  can be explained by the SimCom_NTP_Application_Note.pdf which is available on the internet
 */

handle_status_en Gen_set_timestamp (sim80x_t* sim80x, char* ntp_server, float input_timeout) {
	handle_status_en hstatus;
	clock_t now;

	// Initially the AT+SAPBR="3,1,\"Contype\",\"GPRS\"" is executed
	hstatus = Sim80x_Sapbr(sim80x, "3,1,\"Contype\",\"GPRS\"", input_timeout);
	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()){
		if ((hstatus = Sim80x_Sapbr(sim80x, "3,1,\"Contype\",\"GPRS\"", input_timeout)) == SIM80_OK)
			break;
	}
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;


/* The user has inserted the apn name in the format: "internet.vodafone.gr". The Sim800 Module
 * requires the format: "\"internet.vodafone.g\"". That's why the sprintf's are used. To append
 * the \" characters to the string
 */
	char param[60] = "3,1,\"APN\",";
	sprintf (param + strlen(param), sim80x->GPRS->sim_apn);

	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Sim80x_Sapbr(sim80x, param, input_timeout)) == SIM80_OK)
			break;
	}
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if (Sim80x_flag)	break;
		if ((hstatus = Sim80x_Sapbr(sim80x, "1,1", input_timeout)) == SIM80_OK) {
			Sim80x_flag ++;
			break;
		}
	}
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Gen_BearerProfile(sim80x, "1", input_timeout)) == SIM80_OK)
			break;
	}
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Gen_set_lcl_timestamp (sim80x, input_timeout)) == SIM80_OK)
			break;
	}
	if (hstatus != SIM80_OK)
		return hstatus;

	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Gen_set_ntp_server(sim80x, ntp_server, input_timeout)) == SIM80_OK)
			break;
	}
	if (hstatus != SIM80_OK)
		return hstatus;

	now = clock();
	while (_CLOCK_DIFF(clock(), now) <= GENERAL_TIMEOUT* get_freq()) {
		if ((hstatus = Gen_ntp_sync(sim80x, input_timeout)) == SIM80_OK)
			break;
	}
	if (hstatus != SIM80_OK)
		return hstatus;

	return SIM80_OK;
}


/*!
 * \brief
 *	  With this function we get as response from the Sim80x module the timestamp (current date and time)
 *
 *\param sim80x, pointer to an sim80x_t object
 *\param timestamp, a pointer to a struct in time.h
 *\param input_timeout, the timeout for the receive data function
 *
 *\return handle_status_enumerator
 */
handle_status_en Gen_get_timestamp (sim80x_t* sim80x, struct tm* timestamp, float input_timeout) {
	handle_status_en hstatus;

	char temp_timestamp[25];

	hstatus = Sim80x_send_AT_Command (sim80x, sim80x->AT->_Gen_timestamp,'\r');
	if (hstatus != SIM80_OK)
		return SIM80_SIM_ERROR;

	hstatus = _Sim80x_recv_Data (sim80x, current_command, RESPONSES2, input_timeout);
	if (hstatus != SIM80_OK)
	return hstatus;

	_Sim80x_recv_filter (0); /* Clean the data */

	if (strstr(receivedDataBuffer_fromSim80x, "+CCLK: ") == NULL)
		return SIM80_SIM_ERROR;


	sscanf (receivedDataBuffer_fromSim80x, "+CCLK: \"%s\"", temp_timestamp);
	sscanf (temp_timestamp, "%d/%d/%d,", &timestamp->tm_year, &timestamp->tm_mon, &timestamp->tm_mday);
	char* temp = strstr (temp_timestamp, ",");
	strtok (temp, "+");
	sscanf (temp, ",%d:%d:%d", &timestamp->tm_hour, &timestamp->tm_min, &timestamp->tm_sec);
	timestamp->tm_year += 100;
	timestamp->tm_mon  -= 1;
	return SIM80_OK;
}


